from collections import Counter
from itertools import chain
from random import shuffle

import numpy as np

from storage.db_init import __get_client_dbs
from storage.local import _get_config


def get_n_config():
    config = _get_config()['network']
    training_percentage = config['training_percentage']
    validation_percentage = config['validation_percentage']
    testing_percentage = config['testing_percentage']
    return training_percentage, validation_percentage, testing_percentage


def get_n_params(ms1size, ms2size, unique_labels, ms_level):
    config = _get_config()['network']
    ms2_binning = _get_config()['extractor']['ms2_binning']['apply']
    params = {'ms1size': (ms1size[0], ms1size[1]),
              'ms2size': (ms2size[0]) if ms2_binning else (ms2size[0], ms2size[1]),
              'batch_size': config['batch_size'],
              'n_classes': len(unique_labels),
              'n_channels': config['n_channels'],
              'shuffle': True,
              'ms_level': ms_level,
              'length_ms2': config['length_ms2'],
              'ms2_binning': ms2_binning
              }
    return params


def calc_test_accs(filtered, test_perc):
    test_amount = (test_perc / 100) * filtered.count()
    test_accs = []
    [test_accs.append([f["_id"], f["count"]]) for f in
     list(reversed(list(filtered.aggregate([{'$sortByCount': '$acc'}])))) if
     np.sum([g[1] for g in test_accs]) < test_amount]
    test_accs = [f[0] for f in test_accs]
    return [f for f in test_accs if f is not None]


def train_val_split(accs, test_accs, filtered, train_perc, test_perc):
    train_val_accs = [f for f in accs if f not in test_accs]
    unique_labels = list(np.unique([f['label'] for f in filtered.find({'_id': {'$ne': 'index'}})]))

    if len(unique_labels) > 0.2 * filtered.count():
        classification = False
        train_val_ids = [g['_id'] for g in filtered.find({'_id': {'$ne': 'index'}, 'acc': {'$in': train_val_accs}})]
        shuffle(train_val_ids)
        train = train_val_ids[:int(len(train_val_ids) * (train_perc + test_perc))]
        val = train_val_ids[int(len(train_val_ids) * (train_perc + test_perc)):]

    else:
        classification = True
        train_val_ids = [[g['_id'] for g in filtered.find({'_id': {'$ne': 'index'}, 'label': int(f), 'acc': {'$in': train_val_accs}})] for f in
                         unique_labels]
        shuffle(train_val_ids)
        class_and_split = [[f[int(len(f) * (train_perc + test_perc) / 100) :], f[: int(len(f) * (train_perc + test_perc) / 100)]] for f in train_val_ids]
        train = list(chain.from_iterable([f[1] for f in class_and_split]))
        val = list(chain.from_iterable([f[0] for f in class_and_split]))

    return classification, train, val, unique_labels


def get_class_weights(labels):
    class_counter = Counter(labels.values())
    class_weight = {}
    for f in class_counter:
        class_weight[f] = max(class_counter.values()) / class_counter[f]

    return class_weight


def get_partitions(ms_level):
    train_perc, val_perc, test_perc = get_n_config()
    filtered, non_bin, db, client = __get_client_dbs(None, 'filtered', 'peptides_nonbin')
    ms2binning, binsize = (_get_config()['extractor']['ms2_binning']['apply'], _get_config()['network']['length_ms2'])
    if not ms2binning:
        non_bin.drop()
        non_bin.insert_many([f for f in filtered.find({'_id': {'$ne': 'index'}, 'ms2size': {'$ne': '[500]'}}) if int(f['ms2size'].split(',')[0][1:]) >= binsize])
        filtered = non_bin

    accs = list(np.unique([f['acc'] for f in filtered.find({'_id': {'$ne': 'index'}})]))
    test_accs = calc_test_accs(filtered, test_perc)

    classification, train, val, unique_labels = train_val_split(accs, test_accs, filtered, train_perc, test_perc)

    ms1size, ms2size, name = (eval(filtered.find_one({'_id': {'$ne': 'index'}})['ms1size']),
                              eval(filtered.find_one({'_id': {'$ne': 'index'}})['ms2size']),
                              filtered.find_one({'_id': {'$ne': 'index'}})['name'])

    test = [g['_id'] for g in filtered.find({'acc': {'$in': test_accs}})]

    labels = {}
    [labels.update({f['_id']: f['label']}) for f in filtered.find({'_id': {'$ne': 'index'}})]

    partition = {'train': train, 'validation': val, 'test': test}

    class_weight = get_class_weights(labels)
    params = get_n_params(ms1size, ms2size, unique_labels, ms_level)

    client.close()
    [print(f'{f} datapoints: {len(partition[f])}') for f in partition]
    return partition, labels, ms1size, ms2size, name, classification, len(unique_labels), class_weight, params, test_accs
