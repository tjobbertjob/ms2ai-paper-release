from itertools import chain
from time import time

import numpy as np

from keras.utils import Sequence, to_categorical

from storage.local import _get_config
from storage.serialization import read
from utils.log import ms2ai_log


class DataGenerator(Sequence):
    'Generates data for Keras'

    def __init__(self, path, list_IDs, labels, classification, batch_size, ms1size, ms2size, n_channels,
                 n_classes, shuffle, ms_level, length_ms2, ms2_binning):
        'Initialization'
        self.classification = classification
        self.ms1size = ms1size
        self.ms2size = ms2size
        self.path = path
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.MS = ms_level
        self.minMS2 = length_ms2
        self.on_epoch_end()
        self.ms2_binning = ms2_binning
        self.lstm = _get_config()['network']['ms2_lstm'] == 'True'

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]
        # Generate data
        X, y = self.__data_generation(list_IDs_temp)
        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def get_ms2(self, ms2):
        if self.ms2_binning or self.lstm:
            ms2 = np.array(ms2)
        else:
            sort_lim_ms2 = sorted(sorted(ms2, key=lambda x: x[1], reverse=True)[:self.minMS2],
                                  key=lambda x: x[0])
            ms2 = np.array(list(chain.from_iterable(sort_lim_ms2)))

        return ms2

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples'
        # BRING IN BATCH DATA
        start = time()
        batch = [[read(f'{self.path}{ID}.txt'), self.labels[ID]] for ID in list_IDs_temp]
        if self.MS in ['ms2', 'both'] and not self.ms2_binning:
            ms2_batchlen = max([len(f[0][1]) for f in batch])
        end = time()
        # print(f' Batch load time: {end - start}')

        # Initialize empty sequences
        ms1_zeros = np.empty((self.batch_size, self.ms1size[0], self.ms1size[1], self.n_channels))
        ms2_zeros = np.empty(
            (self.batch_size, ms2_batchlen, self.ms2size[1])) if self.lstm and not self.ms2_binning else np.empty(
            (self.batch_size, 500)) if self.ms2_binning else np.empty(
            (self.batch_size, self.minMS2 * self.ms2size[1]))
        add_info_zeros = np.empty((self.batch_size, 2))

        output_zeros = np.empty(self.batch_size, dtype=int if self.classification else float)

        if self.MS == 'ms2':
            X = ms2_zeros
        else:
            X = ms1_zeros
            X2 = add_info_zeros
            if self.MS == 'both':
                X3 = ms2_zeros

        y = output_zeros

        # Fill empty sequences with batch data
        for i, peptide in enumerate(batch):
            ms1 = peptide[0][0]
            ms2 = peptide[0][1]
            add_info = peptide[0][2]
            labl = peptide[1]
            # MS2 gets flattened in get_ms2
            if self.MS == 'ms2':
                ms2 = self.get_ms2(ms2)
                if self.ms2_binning:
                    X[i,] = ms2
                elif self.lstm:
                    X[i, 0:len(ms2)] = ms2
                else:
                    X[i,] = ms2

            else:
                X[i,] = np.array(ms1)[:, :, 0:self.n_channels]
                X2[i,] = np.array(add_info)
                if self.MS == 'both':
                    ms2 = self.get_ms2(ms2)
                    if self.ms2_binning:
                        X3[i,] = ms2
                    elif self.lstm:
                        X3[i, 0:len(ms2)] = ms2
                    else:
                        X3[i,] = ms2

            y[i] = labl

        if self.classification:
            try:
                y = to_categorical(y, num_classes=self.n_classes)
            except:
                ms2ai_log('Error with to_categorical. Most likely batch_size or test_accessions', 'error')
                quit()

        return ([X, X2], y) if self.MS == 'ms1' else ([X, X2, X3], y) if self.MS == 'both' else (X, y)
