import os
import random

from storage.db_init import __get_client_dbs

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Mutes tensowflow prints
import matplotlib.pyplot as plt
import numpy as np
import wandb
from keras.engine.saving import load_model
from tensorflow.python.keras.utils.vis_utils import plot_model
from sklearn.metrics import confusion_matrix
import seaborn as sns
from network.datagenerator import DataGenerator
from network.model import NetworkModel
from network.get_partitions import get_partitions
from storage.local import _get_config, __get_metapath, __get_path, __get_peptidepath, __create_file


def __performance_plot(history, metric, path, imageclass, n_classes, ms_level):
    plt.plot(history.history[f'{metric}'])
    plt.plot(history.history[f'val_{metric}'])
    plt.title(f'Plot of validation {metric} over time, on {n_classes} class Charge')
    plt.ylabel(f'{metric}')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Validation'], loc='upper left')
    plt.savefig(f'{path}{imageclass.replace("/", "")}_{ms_level}.png')


def __get_performance_plot(history, classification, path, name, n_classes, ms_level):
    if classification:
        __performance_plot(history, 'accuracy', path, name, n_classes, ms_level)
    else:
        __performance_plot(history, 'r2', path, name, n_classes, ms_level)


def keras_model(ms1size, ms2size, n_channels, length_ms2, classification, n_classes, name, metapath, patience,
                ms_level, lstm):
    """
    :param ms1size: Size of ms1 images
    :param ms2size: Size of ms2 arrays.
    :param n_channels: image channels, almost always 4
    :param length_ms2: Length of ms2 used, specified in the config
    :param classification: Whether it's a class or regres problem
    :param n_classes: Self-explanatory
    :param name: name of the class used
    :param metapath: path to metadata
    :param patience: Amount of epochs without improvement before the network automatically stops
    :param ms_level: ms1, ms2 or both
    :return: Trains the neural network described in the model.py file
    """
    # use weights and biases for online tracking.
    weights_and_biases = _get_config()['network']['WandB']
    if weights_and_biases['use_wandb']:
        wandb.init(project=weights_and_biases['project_name'], name=f"{ms_level}" if not (lstm == 'True' and ms_level in ['ms2', 'both']) else f"{ms_level}-lstm")

    model_network = NetworkModel(ms_level, classification, n_classes, ms1size, ms2size, n_channels, length_ms2,
                                 metapath, name, patience)
    model = model_network.get_network()
    callbacks_list = model_network.get_callbacks()
    try:
        plot_model(model, to_file=f'{metapath}model_{ms_level}.png', show_shapes=True)
    except:
        print('Graphviz not installed, cannot save model image')

    if weights_and_biases['use_wandb']:
        model.save(os.path.join(wandb.run.dir, "model.h5"))

    return model, callbacks_list


def print_cm(cm, labels, hide_zeroes=False, hide_diagonal=False, hide_threshold=None):
    """pretty print for confusion matrixes"""
    print(f'Confusion matrix ({round((sum([f[i] for i, f in enumerate(cm)]) / sum([sum(f) for f in cm])) * 100, 3)}%):')
    columnwidth = max([len(x) for x in labels] + [5])  # 5 is value length
    empty_cell = " " * columnwidth
    # Print header
    print("    " + empty_cell*2 + "Predicted")
    print("    " + empty_cell, end=" ")
    for label in labels:
        print("%{0}s".format(columnwidth) % label, end=" ")
    print()
    # Print rows
    for i, label1 in enumerate(labels):
        print("    %{0}s".format(columnwidth) % label1, end=" ")
        for j in range(len(labels)):
            cell = "%{0}.1f".format(columnwidth) % cm[i, j]
            if hide_zeroes:
                cell = cell if float(cm[i, j]) != 0 else empty_cell
            if hide_diagonal:
                cell = cell if i != j else empty_cell
            if hide_threshold:
                cell = cell if cm[i, j] > hide_threshold else empty_cell
            print(cell, end=" ")
        print()


def cm(model, test_generator, test_part):
    filtered, db, client = __get_client_dbs(None, 'filtered')
    y_pred = list(model.predict(test_generator).argmax(axis=-1))
    y_real = [filtered.find_one({'_id': f})['label'] for f in test_part][:len(y_pred)]
    print_cm(confusion_matrix(y_real, y_pred), labels=filtered.find_one({'_id': 'index'})['classes'])


def __test_model(classification, partition, path, mpath, name, labels, **params):
    if len(partition['test']) > 0:
        classification = True if 'c' in str(classification) or classification else False
        model = load_model(f'{mpath}{name}-{params["ms_level"]}.h5')
        params['shuffle'] = False
        for i in reversed(range(1, 256)):
            if len(partition['test']) % i == 0:
                params['batch_size'] = i
                break
        test_generator = DataGenerator(path, partition['test'], labels, classification, **params)

        print(f'Calculating prediction labels', end='\r')
        if classification:
            cm(model, test_generator, partition['test'])
        else:
            test_accuracy = model.evaluate(test_generator)
            print(f'Accuracy on test data. Loss: {test_accuracy[0]}. R^2: {test_accuracy[1]}')


def run_network(*args):
    """
    :param problem: Classification or regression problem (c or r)
    :param ml_class: What class you want to be used. Specified in the peptide filter
    :return: Runs work and tests it

    This function goes through the filtered peptide data and gathers labels and partitions from the config file.
    It runs a network that can be changed in peptide/network/model.py class.

    Can be run on ms1, ms2 or both (see documentation).
    see examples/network.py
    """
    ms_level = args[0]
    test = args[1]
    metapath = __get_metapath()
    imagepath = __get_peptidepath()
    __create_file(__get_metapath())

    (batch_size, epochs, n_channels, training_percentage, validation_percentage, testing_percentage, patience,
     seed, length_ms2, lstm, weights_and_biases) = _get_config()['network'].values()
    setseed = seed['use_seed']

    if setseed:
        random.seed(seed['seed'])
        np.random.seed(seed['seed'])
        os.environ['PYTHONHASHSEED'] = str(seed['seed'])
        # set_seed(1)

    partition, labels, ms1size, ms2size, name, classification, n_classes, class_weight, params, test_accs = get_partitions(ms_level)

    if test:
        return __test_model(classification, partition, imagepath, metapath, name, labels, **params)
        quit()

    training_generator = DataGenerator(imagepath, partition['train'], labels, classification, **params)
    validation_generator = DataGenerator(imagepath, partition['validation'], labels, classification, **params)

    model, callbacklist = keras_model(ms1size, ms2size, n_channels, length_ms2, classification, n_classes, name,
                                      metapath, patience, ms_level, lstm)

    history = model.fit(training_generator, validation_data=validation_generator, epochs=epochs,
                        callbacks=callbacklist, class_weight=class_weight)

    __get_performance_plot(history, classification, metapath, name, n_classes, ms_level)
    return __test_model(classification, partition, imagepath, metapath, name, labels, **params)

# python3 run.py r m/z
# python3 run.py c Length_class
# python3 run.py c Seq_class
# python3 run.py c Modi_class
