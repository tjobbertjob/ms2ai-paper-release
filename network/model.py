from keras.layers import Dense, Input, Conv2D, MaxPooling2D, Concatenate, GlobalAveragePooling2D, \
    GlobalAveragePooling1D, LSTM, Dropout, Flatten, Lambda
from keras.models import Model

from keras.callbacks import ModelCheckpoint, EarlyStopping
import keras.backend as K

from network.metrics import r2_metric
from storage.local import _get_config


class NetworkModel:
    def __init__(self, ms_level, classification, n_classes, ms1size, ms2size, n_channels, lenms2, metapath, name,
                 patience):
        self.patience = patience
        self.name = name
        self.metapath = metapath
        self.lenMS2 = lenms2
        self.n_channels = n_channels
        self.ms2size = ms2size
        self.ms1size = ms1size
        self.ms_level = ms_level
        self.classification = classification
        self.n_classes = n_classes
        self.lstm = _get_config()['network']['ms2_lstm'] == 'True'

    def phase1_ms1(self):
        input_l = Input(shape=(self.ms1size[0], self.ms1size[1], self.n_channels,), name='ms1_input')
        kernels = [(2, 2), (3, 3), (5, 5), (7, 7), (10, 10)]
        x = Conv2D(256, kernel_size=kernels[1], activation='relu', padding='same', name=f'ms1_conv_{str(kernels[1][1])}_1')(input_l)
        x = MaxPooling2D(pool_size=(2, 2), strides=(1, 1), padding='same')(x)

        x1 = Conv2D(256, kernel_size=kernels[2], activation='relu', padding='same', name=f'ms1_conv_filt{str(kernels[2][1])}_1')(input_l)
        x1 = MaxPooling2D(pool_size=(2, 2), strides=(1, 1), padding='same')(x1)

        x2 = Conv2D(256, kernel_size=kernels[3], activation='relu', padding='same', name=f'ms1_conv_filt{str(kernels[3][1])}_1')(input_l)
        x2 = MaxPooling2D(pool_size=(2, 2), strides=(1, 1), padding='same')(x2)

        x3 = Conv2D(256, kernel_size=kernels[4], activation='relu', padding='same', name=f'ms1_conv_filt{str(kernels[4][1])}_1')(input_l)
        x3 = MaxPooling2D(pool_size=(2, 2), strides=(1, 1), padding='same')(x3)

        x = Concatenate()([x, x1, x2, x3])
        # x = Lambda(lambda x: K.mean(x, axis=3), name='filter_mean')(x)
        # x = Flatten()(x)
        x = GlobalAveragePooling2D(name='ms1_GAP')(x)  # data_format='channels_first'

        return input_l, x

    def phase1_add_info(self):
        input_l = Input(shape=(2), name='additional_input')
        x = Dense(64, activation='relu', name='addtional_dense')(input_l)

        return input_l, x

    def phase1_ms2(self):
        ms2_binning = _get_config()['extractor']['ms2_binning']['apply']
        if self.lstm:
            input_l = Input(shape=(self.ms2size[0], 1) if ms2_binning else (None, 2), name='ms2_lstm_input')  # (Seq_len, 2) 2:[m/z, int]
            x = LSTM(256)(input_l)
            x = Dense(256, activation='relu', name='ms2_lstm_dense_1')(x)
            x = Dense(256, activation='relu', name='ms2_lstm_dense_2')(x)
            x = Dense(256, activation='relu', name='ms2_lstm_dense_3')(x)
        else:
            input_l = Input(
                shape=(self.ms2size[0]) if ms2_binning else (2 * self.lenMS2), name='ms2_input')  # (2, #ms2_peaks) 2:[m/z, int]
            # x = Dropout(rate=0.2)(input_l)
            x = Dense(512, activation='relu', name='ms2_dense_1')(input_l)
            # x = Dropout(rate=0.1)(x)
            x = Dense(512, activation='relu', name='ms2_dense_2')(x)
            # x = Dropout(rate=0.1)(x)
            x = Dense(512, activation='relu', name='ms2_dense_3')(x)
            x = Dense(512, activation='relu', name='ms2_dense_4')(x)
            x = Dense(256, activation='relu', name='ms2_dense_5')(x)
            x = Dense(128, activation='relu', name='ms2_dense_6')(x)

        return input_l, x

    def phase2_comb(self):
        network_input = Dense(128, activation='relu', name='comb_dense_1')(self.p1_output_layer)
        x = Dense(128, activation='relu', name='comb_dense_2')(network_input)
        x = Dense(128, activation='relu', name='comb_dense_3')(x)
        network_output = Dense(64, activation='relu', name='comb_dense_4')(x)
        return network_output

    def phase3_comb(self):
        if self.classification:
            return Dense(self.n_classes, activation='softmax', name='output_softmax')
        else:
            return Dense(self.n_classes, activation='linear', name='output_linear')

    def get_network(self):
        if self.ms_level == 'ms1':
            (ms1_input, ms1_output) = self.phase1_ms1()
            (add_input, add_output) = self.phase1_add_info()
            self.p1_input_layer = [ms1_input, add_input]
            self.p1_output_layer = Concatenate()([ms1_output, add_output])


        elif self.ms_level == 'ms2':
            (self.p1_input_layer, self.p1_output_layer) = self.phase1_ms2()

        else:
            (ms1_input, ms1_output) = self.phase1_ms1()
            (add_input, add_output) = self.phase1_add_info()
            (ms2_input, ms2_output) = self.phase1_ms2()
            self.p1_input_layer = [ms1_input, add_input, ms2_input]

            self.p1_output_layer = Concatenate()([ms1_output, add_output, ms2_output])

        p2_output_layer = self.phase2_comb()

        self.p3_output_layer = self.phase3_comb()(p2_output_layer)

        self.model = Model(inputs=self.p1_input_layer, outputs=self.p3_output_layer)

        if self.classification:
            self.model.compile(loss='binary_crossentropy', metrics=['accuracy'],
                               optimizer='rmsprop' if self.lstm and self.ms_level == 'ms2' else 'adam')
        else:
            self.model.compile(loss='mse', metrics=[r2_metric], optimizer='rmsprop')

        return self.model

    def get_callbacks(self):
        checkpoint = ModelCheckpoint(f'{self.metapath}{self.name}-{self.ms_level}.h5',
                                     monitor='val_accuracy' if self.classification else 'val_r2',
                                     save_best_only=True)
        early_stopping = EarlyStopping(monitor='val_loss', patience=self.patience)
        wandb_init = _get_config()['network']['WandB']
        WandB = wandb_init['use_wandb']
        if WandB:
            import wandb
            from wandb.keras import WandbCallback
            wandb.init(project=wandb_init['project_name'])

        return [checkpoint, early_stopping, WandbCallback()] if WandB else [checkpoint, early_stopping]

