# MS2AI: Mass Spectrometry Pipeline

**Documentations are available in the *documentation.pdf* and on the GitLab Wiki!**

Pipeline to get Mass spectrometery data into Machine learning applications.
The Pipeline does everything from data acquisition, data extraction and machine learning applications, 
making it the one stop software for MS and AI implementations.

The reason data acquisition is important, is that machine learning applications needs a lot of homogenous data 
in order to be trained, which is hard to get from in-house data alone.

The pipeline is split into 4 main parts: (1) PRIDE Project metadata acquisition, 
(2) Data Extraction, (3) Metadata filtering, and (4) Machine learning application.

These parts are also separated code wise, as the potential (and average) 
runtime of the individual parts can take days or weeks to complete.

The software uses MaxQuant output files to find peptides in the MS run, 
and MaxQuant analysis of the MS data is thus a necessity for the software to be used.

## Requirements
OS: Linux, MacOS or Windows with Python 3.8

MongoDB: A working version of MongoDB with the server running

Formatting Tool: Requires a working version of Conda (MacOS or Linux) or Docker

## Installation
### Python 3.6 Packages
```
📦ms2ai
 ┣ 📂extactor
 ┣ 📂filter
 ┣ 📂network
 ┣ 📂scraper
 ┣ 📂storage
 ┣ 📂summary
 ┣ 📂tutorial
 ┣ 📂utils
 ┣ 📂tutorial
 ┣ 📜config.json 
 ┣ 📜readme.md 
 ┣ 📜requirements.txt ← 
 ┗ 📜Documentation.pdf
```
*For certain packages a c-compiler is necessary (gcc) which is in build-essential on MacOS or Linux*

`pip install -r requirements.txt`

### MongoDB

#### Windows:
Follow tutorial from [here](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/), and make sure to 
also install the [mongo-tools](https://www.mongodb.com/try/download/database-tools) and add this to the path varaibles, to be able to download the current PRIDE metadata version.



#### Linux

`sudo apt-get install mongodb` 

`sudo service mongodb start`

#### MacOS
###### Requires Homebrew installation. Skip first line if Homebrew is already installed
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)
brew install node
brew install mongo
sudo mkdir -p /data/db
sudo chmod -arwx /data/db
```

### Get current PRIDE metadata database collection
The in-built method will pull the current version of the collection, and import it into the 'ms2ai' database:
```
python api.py -db
```

### Docker or Conda
#### Conda
##### Linux
`wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`

##### MacOS
`wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh`

##### Install
```
sh Miniconda3-latest-Linux-x86_64.sh
pip install conda
```

##### Add channels
```
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
conda install -y -c bioconda thermorawfileparser
```

#### Docker

Install Docker and pull this image

```
docker pull veitveit/trp
pip install docker
```

## Data-path directory structure
```
📦/path/to/data
 ┣ 📂images
 ┃   ┗📂database-name
 ┃      ┗📂PXD projects
 ┃         ┗📂raw files
 ┃            ┗📜peptide-representation-ID.txt
 ┣ 📂metadata
 ┃   ┣📜best-model.h5
 ┃   ┣📜network-model.png
 ┃   ┗📜historic-plot.png
 ┣ 📂PXD projects
 ┃   ┗📂raw files
 ┃      ┣ 📜file.raw
 ┃      ┣ 📜file.mzML.gz
 ┃      ┣ 📜mzML.txt
 ┃      ┣ 📜MaxQuant.txt
 ┗      ┗ 📜XxY.txt
```
In the configuration file there is a path parameter which must be set to an already created folder. 
This can be an absolute or relative path on the local machine, but must exist pre running the software.

Within this directory will be created directories for all projects extracted, aswell as a metadata and an image directory.

The image directory will be the palce where all peptide .txt files are saved, and is automatically pulled 
into the machine learning application.

The metadata holds the information from the neural networks: the history loss plots, the model overviews, 
and the model saved model and weights. This is to be used if you want to check how a neural networks is performing
and share the results with others. This is also where models should be placed if you want to test others models
using our software.


## Usage:
```
📦ms2ai
 ┣ 📂extactor
 ┣ 📂filter
 ┣ 📂network
 ┣ 📂scraper
 ┣ 📂storage
 ┣ 📂summary
 ┣ 📂tutorial 
 ┣ 📂utils
 ┣ 📂tutorial
 ┣ 📜config.json 
 ┣ 📜readme.md 
 ┣ 📜requirements.txt 
 ┗ 📜Documentation.pdf ←
```
MS2AI comes with an indepth documentation, 
that illustrates all the possible use cases of the software with examples in the documentation.pdf

## Tutorial
```
📦ms2ai
 ┣ 📂extactor
 ┣ 📂filter
 ┣ 📂network
 ┣ 📂scraper
 ┣ 📂storage
 ┣ 📂summary
 ┣ 📂tutorial ←
 ┣  ┣📜tutorial.py ←
 ┣  ┣📜tutorial.ipynb ←
 ┣  ┗📜tutorial.h5
 ┣ 📂utils
 ┣ 📂tutorial
 ┣ 📜config.json 
 ┣ 📜readme.md 
 ┣ 📜requirements.txt 
 ┗ 📜Documentation.pdf
```

####This tutorial is performed by:

`python ms2ai_api.py -T`

#####What is needed before running this:

<ul>
    <li>Get Python 3.6+</li>
    <li>Setup MongoDB</li>
    <li>Get Conda or Docker</li>
    <li>Set path and format_software configuration parameters</li>
</ul>

The tutorial will install the necessary python packages and install/build ThermoRawFileParser in the software chosen.
(if it hasn't been manually installed)

To see and understand what happens read the tutorial part of the supplementary paper, 
or look into the given tutorial files.

## Configuration file
```
📦ms2ai
 ┣ 📂extactor
 ┣ 📂filter
 ┣ 📂network
 ┣ 📂scraper
 ┣ 📂storage
 ┣ 📂summary
 ┣ 📂tutorial
 ┣ 📂utils
 ┣ 📂tutorial
 ┣ 📜config.json ←
 ┣ 📜readme.md 
 ┣ 📜requirements.txt 
 ┗ 📜Documentation.pdf
```

*This is the file in which you specify how you want the project to run*

**All values have default _except_ for the path which must direct to an existing folder, 
and the formatting tool (format_software) must be set to the one installed**

#### Storage attributes:
* path: Where all data gets put to - Need for large storage (end path with "/")

* db_name: What MongoDB name to use. This is used to create separate peptide for different projects or
of differing sizes to test, without having to remove old peptides. Simply rename this to something else 
and the project will still run anew. Also, the PRIDE metadata will always be in the 'ms2ai' database
 to ensure consistency with the import of the provided db. - Defaults to 'ms2ai'

* pride_archive: Just a place to keep and update pride archive URL 

* pride_project: Just a place to keep and update pride project URL 
	
* pride_files: Just a place to keep and update pride file URL 


#### Extraction attributes:
* maxquant_file: The given file from which peptides are to be taken; allPeptides.txt, evidence.txt or msms.txt. 
defaults to allPeptides.txt

* format_software: What software is used for formatting from .raw to .mzml. 
Possibilies are "conda" or "docker" so the used method must be installed - Defaults to "conda"

* thermo_peak_picking: Whether or not to utilize the native thermo software peak picking when formatting from .raw to .mzML -
Defaults to true

* maxquant_file: What output file from MaxQuant should be utilized in finding peptides in the lc-ms run.
Possible values are "allPeptides.txt" or "evidence.txt" - Defaults to "allPeptides.txt"

* mz_bin: Size of m/z bins used for full ms1 image - Defaults to 0.75

* rt_bin: Size of retention time bins used for full ms1 image - Defaults to 0.06

* mz_interval: m/z interval used for peptide neighbourhood around the peptides - Defaults to +-25

* rt_interval: rt interval used for peptide neighbourhood around the peptides - Defaults to +-5

* remove_used_files: Removing the raw files, .mzML.gz and intermediary and metadata files when they 
are no longer useful in the pipeline. Saves large amounts of storage space - Defaults to true.

* ms2_binning: A dictionary contaning information about methods used for ms2 binning, 
               beware that changing these parameters will make current data imcompatible with future data. 
               The paramters are:
    * apply: Whether or not to apply binning to MS2 spectra. Defaults to true
    * retain_int: Whether or not to retain intensity information from MS2 spectra. Defaults to true
    * fixed_mz: Whether to have fixed or variable lengths of ms2 binning space. Defaults to true
    * fixed_mz_length: if fixed_mz is true, this parameter is the maximum mz value. Defaults to 2500
    * bin_amount: How many bins to use for each MS2 Spectra. defaults to 500.    

* acquire_only_new: A boolean value for skipping PRIDE projects if you have 
already created all images - Defaults to true

* multi_processing: A Boolean value for running the extraction multithreaded 
(reduces error messages) - Defaults to true

* nr_processes: Amount of threads used for multi threading - Defaults to 10

* save_images_as_png: Whether or not to save images as png files or only txt - Dafults to false

* receive_traceback: If the code fails, do you want to have a traceback as to how to fix it 
(entire extactor is wrapped in try-catch). Defaults to false

#### filtering attributes:
* binary_in_all_rawsfiles: Whether or not the binary class used for filtering, should be present
in all the rawfiles. fx if we're searching for Phosphorylated peptides, that we dont take the data from 
rawfiles, where there is no phosphorylated peptides at all, as this could be because they didn't
search for phosphorylated peptides in the study, and not that phosphorylation isnt present. - Defaults to true

* limit_data: Whether or not to fix class imbalance with reduced data amount, 
reducing all classes to that of the least abundant class - Defaults to true

* amounts_of_classes: How many classes is wanted in the filtered version, taken by the most abundant classes. 
defaults to 2

* filterscore: When filtering subimage metadata, do you want to have a score amount that the 
peptide has to achieve, and what is the percentile of all score you want to have above. - Defaults to [false, 50]

#### network attributes:
* batch_size: Size of batches for stochastic gradient descent - Defaults to 64

* epochs: Max epochs used for neural network training - Defaults to 100

* n_channels: Number of information channels used in neural network training - Defaults to 4

* training_percentage: The amount of data going to training - Defaults to 85

* validation_percentage: The amount of data going to validating - Defaults to 10 

* testing_percentage: The amount of data going to testing. 
This is done on accessions basis, meaning if set to any amount higher than 0, 
it will require data from an entire accession. 
This is done to get data that is NOT like any seen in testing - Defaults to 5

* early_stopping: Number of epochs with no improvement before stopping - Defaults to 10

* setseed: Makes sure that it always initializes files in the same training and test groupings, if this is set to false, 
the test groups will change from run to run. Defaults to true

* length_ms2: **_Only used if binning was NOT used during extration_**. 
it takes only peptides with more ms2 peaks the length given here (Necessity for the neural network). 
the peaks are chosen based on highest intensity. defaults to 100

* ms2_lstm: Whether to use an LSTM (long-short term memory) for ms2 (if not an FNN is used).
Only for advances users with knowledge of neural networks. Defaults to false

* use_WandB: Whether or not to send the neural network training to service weights and biases; wandb.com. Defaults to false


## Usage:
```
📦ms2ai
 ┣ 📂extactor
 ┣ 📂filter
 ┣ 📂network
 ┣ 📂scraper
 ┣ 📂storage
 ┣ 📂summary
 ┣ 📂tutorial 
 ┣ 📂utils
 ┣ 📂tutorial
 ┣ 📜config.json 
 ┣ 📜readme.md 
 ┣ 📜requirements.txt 
 ┗ 📜Documentation.pdf ←
```
#### *Tutorial is available in the tutorial directory*

Exists both in a python file (_tutorial/tutorial.py_) or a Jupyter notebook (_tutorial/tutorial.ipynb_)

The software includes a complete pipeline from data acquisition to a machine learning application.
Due to this the usage is split into seperate components, all combined in the ms2ai_api.

#### PRIDE Data Acquistion:
Goes through the PRIDE API and appends all project metadata to mongodb collection.

The PRIDE Api can be updated through the config files in case of change, but should be updated through the official ms2ai gitlab

`python ms2ai_api.py pride`

Is possible to run either as continuation from previous collection or restart from beginning (through input during script).


#### Peptide Extractor:
Goes through MS Rawfiles accompanied by MaxQuant files. This has a range of uses further specified 
in the documentation.pdf. Put shortly, it can do pride projects or local projects in a range of different ways.

The software utilizes a formatting tool call [ThermoRawFileParser](https://github.com/compomics/ThermoRawFileParser)
which requires either Docker or Conda installation (see **Installation**) to be performed.

`python ms2ai_api.py extractor *method*`

Find methods in documentation.pdf

#### Peptide Filter:
Filters the peptide metadata and prepares a MongoDB collection for Machine learning applications.

The output is a separate mongoDB collection with only necessary information on the peptides.
This collection is specialized for the data generator in the neural network released with the software.

`python ms2ai_api.py filter *class* *binary_class*`

More specifications on class and binary_class in documentation.pdf

#### Neural Network Application:
A base neural network implementation. It has the potential to use MS1, MS2 or a combination of the two as the inputs for the neural network.

This implementation is a rather rudimentary one, and modifications can be done in the network.model file

`python ms2ai_api.py network *ms_level* *test*`

More specifications on ms_level and test in documentation.pdf


## Tutorial
```
📦ms2ai
 ┣ 📂extactor
 ┣ 📂filter
 ┣ 📂network
 ┣ 📂scraper
 ┣ 📂storage
 ┣ 📂summary
 ┣ 📂tutorial ←
 ┣  ┣📜tutorial.py ←
 ┣  ┣📜tutorial.ipynb ←
 ┣  ┗📜tutorial.h5
 ┣ 📂utils
 ┣ 📂tutorial
 ┣ 📜config.json 
 ┣ 📜readme.md 
 ┣ 📜requirements.txt 
 ┗ 📜Documentation.pdf
```

####This tutorial is performed by:

`python ms2ai_api.py tutorial`

#####What is needed before running this:

<ul>
    <li>Get Python 3.6+</li>
    <li>Setup MongoDB</li>
    <li>Get Conda or Docker</li>
    <li>Set path and format_software configuration parameters</li>
</ul>

The tutorial will install the necessary python packages and install/build ThermoRawFileParser in the software chosen.
(if it hasn't been manually installed)

To see and understand what happens read the tutorial part of the supplementary paper, 
or look into the given tutorial files.
