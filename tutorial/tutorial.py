from sys import platform
import os
import shutil
from storage.local import __get_metapath


def full_tutorial():
    """
    The first part of the tutorial will be to go through the README.md file, as it contains a lot of useful and necessary information.
    The first step in here is to either create a folder called Data in the base of the git repo, or to change the path parameter from the config file.

    The next is to setup the formatting tool. As stated in the readme, if youre on Mac or Linux you can utilize the easier Conda version (still requires Conda)
    and if youre on Windows (or a 4th option) you have to use Docker. The Conda package or the Docker image installation is shown in the readme under installation,
    but will also automatically be installed during the extraction, as long as the 'format_software' is specified correctly.

    The next step is to get the necessary python packages. This is done through the requirements.txt file from the base, and is installed below.
    """
    print('TUTORIAL: installing pre-required python packages')
    os.system(f'pip install -r requirements.txt {">nul" if platform.startswith("win") else ">/dev/null 2>&1"}')

    """
    The next step is to actually start creating the data, this is extractor part of the pipeline.
    Extraction:
    We want to start by getting peptide data from a single PRIDE project. 
    
    To run known PRIDE projcets there is no requirement to gather all pride metadata, and therefore this scraper step is skipped.
    If we wished to run that step anyways (this normally takes a day or two from start to completion) 
    we could do it by running 'python api.py pride'
    
    We have chosen the accession PXD019564, as it has only one rawfile and is relative small. This will speed the process 
    of downloading, formatting and extracting much faster, ideal for the tutorial purposes.
    
    This step created the folders for the project and raw files in the specifed data path.
    It then downloads, formats and extracts necessary information about MS1 and MS2 from the raw files.
    The next and last step is to create files for each peptide found within the MaxQuant file specified in the output 
    (we use allPeptides.txt for this tutorial) 
    """
    print('TUTORIAL: Retrieving data from pride accession PXD019564')
    os.system('python api.py -e PXD019564')

    """
    Filtering:
    The tutorial is set up after the usecase we presented in the paper, which is destinguishing between oxidised and non oxidised peptides
    For this reason we filter the extracted peptide metadata accordingly, so that our data fits the pre-trained network.
    
    The filtered data goes into the 'ms2ai' MongoDB in the 'filtered' collection. 
    """
    print('\nTUTORIAL: Filtering data based on score and PTMs (oxidation)')
    os.system('python api.py -f Modifications -b "Oxidation (M)"')

    """
    Network:
    Lastly we want to test the data against the network provided. 
    this will not train any neural network, simply use the pre-trained one, and see how well that does on our tutorial data.
    
    In order to train this, we have to create the necessary paths (see *Folders and Paths* section of Readme), 
    and copy the network file to the right directory for it to be read by Keras.
    """
    print('\nTUTORIAL: creating file system and running a neural network test on PTM data')
    shutil.copy('tutorial/tutorial.h5', __get_metapath() + 'Modifications-both.h5')
    os.system('python api.py -n both -t')

    """
    Run in single -tag format (requires the model file to be in the right location): 
    python api.py -e PXD019564 -f Modifications -b "Oxidation (M)" -n both -t
    """
