from collections import defaultdict, Counter
from itertools import chain
from time import time, sleep

from filter.classifications import __classification
from filter.regressions import __regression
from storage.db_init import __get_client_dbs
from storage.local import _get_config
import numpy as np


def __set_config():
    config = _get_config()['filter']
    binary_in_all_rawsfiles = config['binary_in_all_rawsfiles']
    score_filter = config['add_score_filter'][0]
    perc = config['add_score_filter'][1] if score_filter else 0
    limit = config['limit_data']
    return binary_in_all_rawsfiles, perc, limit


def __get_score_percentile(pep_count, peptides, perc):
    if pep_count > 1000000:
        Scores = [f['Score'] for f in peptides.find({}, {"Score": 1, '_id': 0})[:1000000]]
    else:
        Scores = [f['Score'] for f in peptides.find({}, {"Score": 1, '_id': 0})]
    perc = np.percentile(Scores, perc)
    ms1size = peptides.find_one()['ms1size']
    return perc, ms1size


def __inclusive_rawfiles(binary_class, cla, peps, amount_classes):
    class_dict = defaultdict(list)
    [class_dict[f'{f["accession"]}/{f["Raw file"]}'].append(f[cla]) for f in peps]  # if not f[cla] in a[f['Raw file']]]

    classes = list(Counter(chain(*class_dict.values())).keys())[:amount_classes]

    bin_raws = [f for f in class_dict if set(np.unique(class_dict[f])) - {binary_class}] if binary_class else \
        [f for f in class_dict if all(item in np.unique(class_dict[f]) for item in classes)]

    if not bin_raws:
        print('Error: No raw files contain all specified filtering classes! Lower class amount or set "binary_in_all_rawsfiles" to false')
        quit()

    return [f for f in peps if f'{f["accession"]}/{f["Raw file"]}' in bin_raws]


def run_filter(*args):
    """
    :param cla: Whatever main class is used for filtering
    :param binary_cl: if given, what class should be used as the '1' class of a binary classifier
    :return: a filtered version of the mongodb collection. Used for the neural network part of the software.

    This software will go through the peptides collection and filter away and peptide that dont reach the requirements
    set by either the sys inputs or the configuration file.
    See documentation on how to use
    """
    start = time()
    cla = args[0]
    binary_cl = args[1]
    amount_classes = args[2]

    if binary_cl is None and amount_classes is None:
        print('Error: Specify either amount of classes or binary class!')
        quit()

    binary_in_all_rawsfiles, perc, limit = __set_config()
    pride, db, client = __get_client_dbs('ms2ai', 'pride')
    peptides, filtered, db, client = __get_client_dbs(None, 'peptides', 'filtered')

    pep_count = db.command("collstats", "peptides")["count"]
    print(f'Total peptides: {"{:,}".format(pep_count)}. Estimated peptide after filtering: {"{:,}".format(int(pep_count * (1 - (perc / 100))))}')
    print(f'Estimated runtime: {round(pep_count * (1 - (perc / 100)) * 7.168494e-05, 2)}s or {round(pep_count * (1 - (perc / 100)) * 1.198203e-04 / 60, 2)}m')
    print(f'Calculating Score percentiles - Time: {round(time()-start,2)}', end='\r')
    perc, ms1size = __get_score_percentile(pep_count, peptides, perc)

    print(f'Retrieving list of valid peptides - Time: {round(time()-start,2)}', end='\r')
    # percentiles = {}
    # all_projects = peptides.find().distinct('accession')
    # [percentiles.update({f: np.percentile([g['Score'] for g in peptides.find({'accession': f}, {'_id': 0, 'Score': 1})], perc)}) for f in all_projects]
    peps = list(peptides.find({'Score': {'$gt': perc}, 'ms1size': ms1size}, {'_id': 1, 'accession': 1, 'Raw file': 1, 'ms1size': 1, 'Score': 1, 'ms2size': 1, cla: 1}))

    # Update peptide list to only those is raw files where all classes exists
    if binary_in_all_rawsfiles:
        print(f'Removing raw files without requires classes - Time: {round(time()-start,2)}', end='\r')
        peps = __inclusive_rawfiles(binary_cl, cla, peps, amount_classes)

    print(f'Writing peptides to filtered DB - Time: {round(time()-start,2)}', end='\r')
    # The program is now ready to start creating a new filtered version, so it drops the old
    filtered.drop()
    # Regression filter of m/z
    if cla in ['m/z']:
        __regression(cla, peps, filtered, db)

    # Classification filter
    else:
        __classification(cla, amount_classes, limit, binary_cl, peps, filtered, pride)

    print(f'Time elaborated: {round((time()-start) / 60, 3)}min '
          f'| Per peptides: {"{:e}".format(np.mean([((time()-start) / len(peps)), (time()-start) / db.command("collstats", "peptides")["count"]]))} seconds')
