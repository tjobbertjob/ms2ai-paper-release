from collections import Counter
from itertools import chain
from random import shuffle
import numpy as np


def __instrument_classes(list):
    for elements in list:
        if 'q exactive' in elements.lower():
            return 'Q Exactive'
        elif 'ltq' in elements.lower() or 'orbitrap' in elements.lower():
            return 'LTQ Orbitrap'
        elif 'tof' in elements.lower():
            return 'TOF'
    return 'other'


def __pride_filter(cla, peptides, pride, amount_classes, binary_class):
    cla = cla.split('-')[1]
    a1 = Counter([f['accession'] for f in peptides])
    pride_dict = {}
    [pride_dict.update({f['accession']: __instrument_classes(f[cla]) if cla == 'instrumentNames' else f[cla]}) for f in
     pride.find({'instrumentNames': {'$exists': True}})]
    a2 = Counter(list(chain.from_iterable([[pride_dict[f]] * a1[f] for f in a1]))).most_common()[:amount_classes]
    b = [f[0] for f in a2]

    if binary_class is None:
        lists = [[{'_id': f['_id'], 'label': b.index(g), 'acc': f['accession'], 'ms1size': f['ms1size'],
                   'ms2size': f['ms2size'], 'name': cla} for f in peptides if
                  pride_dict[f['accession']] == g] for g in b]

    else:
        l1 = [{'_id': f['_id'], 'label': 1, 'acc': f['accession'], 'ms1size': f['ms1size'], 'ms2size': f['ms2size'],
               'name': cla} for f in peptides if pride_dict[f['accession']] == binary_class]
        l2 = [{'_id': f['_id'], 'label': 0, 'acc': f['accession'], 'ms1size': f['ms1size'], 'ms2size': f['ms2size'],
               'name': cla} for f in peptides if pride_dict[f['accession']] != binary_class]
        lists = [l1, l2]

    return lists, b


def __special_sequence_case(cla, binary_class, peptides):
    chars = binary_class.split('or')
    l1 = [{'_id': f['_id'], 'label': 0, 'acc': f['accession'], 'ms1size': f['ms1size'], 'ms2size': f['ms2size'],
          'name': cla} for f in peptides if not any(i in f[cla] for i in chars)]
    l2 = [{'_id': f['_id'], 'label': 1, 'acc': f['accession'], 'ms1size': f['ms1size'], 'ms2size': f['ms2size'],
          'name': cla} for f in peptides if any(i in f[cla] for i in chars)]

    return l1, l2


def __peptide_filter(cla, peptides, amount_classes, binary_class):
    a = [f[0] for f in Counter([f[cla] for f in peptides]).most_common()]
    if binary_class is None:
        a = a[:amount_classes]
        lists = [[{'_id': f['_id'], 'label': a[:amount_classes].index(g), 'label_name': g, 'acc': f['accession'], 'ms1size': f['ms1size'],
                   'ms2size': f['ms2size'], 'name': cla} for f in peptides if f[cla] == g] for g in a]

    else:
        binary_non_similar = True
        if binary_non_similar:  # Removes all the similar classses (fx if binary class is Phospho (STY) and one has (Oxidation (M), Phospho (STY)), this is removed).
            a = [f for f in a if f == binary_class or binary_class not in f]
        if cla == 'Sequence' and 'or' in binary_class:
            l1, l2 = __special_sequence_case(cla, binary_class, peptides)

        else:
            l1 = [{'_id': f['_id'], 'label': 1, 'acc': f['accession'], 'ms1size': f['ms1size'], 'ms2size': f['ms2size'],
                   'name': cla} for f in peptides if f[cla] == binary_class]
            l2 = [{'_id': f['_id'], 'label': 0, 'acc': f['accession'], 'ms1size': f['ms1size'], 'ms2size': f['ms2size'],
                   'name': cla} for f in peptides if f[cla] != binary_class and f[cla] in a]

        a = list(np.unique([binary_class if f == binary_class else f'Not {binary_class}' for f in a]))
        lists = [l1, l2]

    return lists, a


def __classification(cla, amount_classes, limit, binary_class, peptides, filtered, pride):
    if 'pride' in cla:
        lists, b = __pride_filter(cla, peptides, pride, amount_classes, binary_class)

    else:
        lists, b = __peptide_filter(cla, peptides, amount_classes, binary_class)

    if not lists:
        print('No peptides fulfill the requirements, make sure that the right database and classes are chosen!')
        quit()

    [shuffle(f) for f in lists]
    if limit:
        lists = [f[:min([len(f) for f in lists])] for f in lists]

    if binary_class is None:
        [print(f'{b.index(f)}: {f} ({len(h)})                      ') for f, h in zip(b, lists)]
    else:
        [print(f'{f}: {g} ({len(h)})                          ') for f, g, h in zip([1, 0], [binary_class, f'Not {binary_class}'], lists)]
    filtered.insert({'_id': 'index', 'classes': b})
    [filtered.insert_many(f) for f in lists]