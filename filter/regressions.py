
def __regression(cla, peptides, filtered, db):
    lists = [{'_id': f['_id'], 'label': f[cla], 'acc': f['accession'], 'ms1size': f['ms1size'], 'ms2size': f['ms2size'], 'name': cla} for f in
             peptides]
    filtered.insert_many(lists)
    print(f'Peptides in filtered version: {db.command("collstats", "peptides")["count"]}')