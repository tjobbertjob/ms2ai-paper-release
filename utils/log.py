import logging


def __internal_print(string, multiprocessing, overwrite=True):
    if not multiprocessing:
        if overwrite:
            print(f'{string}                                       ', end='\r')
        else:
            print(f'{string}                                       ')


def __logger_config():
    logging.basicConfig(level=logging.INFO)


def ms2ai_log(msg, level):
    """
    :param msg: message to be logged
    :param level: What severity the message has, check logging documentation
    :return: Logs the message at the level specified.

    It also creates the logger from logger config. without doing this the logger refuses to work
    """
    __logger_config()
    logging.log(logging._checkLevel(level.upper()), msg)
