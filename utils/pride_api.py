from scraper.common import __get_json_page
from storage.local import _get_config
import numpy as np


def project_api(accession):
    project_url = _get_config()['storage']['pride_project'].replace('PXDxxxxxx', accession)
    return __get_json_page(project_url)


def file_api(accession, sleep_time=4.5):
    project_url = _get_config()['storage']['pride_files'].replace('PXDxxxxxx', accession)
    return __get_json_page(project_url, sleep_time)


def get_api_filetypes(json_api):
    def requirement(file):
        return file['fileCategory']['value'] in ['RAW', 'SEARCH']
    all_files = [f['fileName'].split('.')[-1] for f in json_api if requirement(f)]
    return list(np.unique(all_files))


def get_api_zipfiles(json_api, ziplist):
    def api_requirement(file_list, ziplist):
        return any([file_list['fileName'].endswith(f) for f in ziplist]) and file_list['fileCategory']['value'] == 'SEARCH'
    zipfiles = []
    for f in json_api:
        if api_requirement(f, ziplist):
            link = [g['value'] for g in f['publicFileLocations'] if 'ftp' in g['value']][0]
            size = f['fileSizeBytes']
            zipfiles.append([link, size])

    return zipfiles