import shutil

from storage.db_init import __get_client_dbs
from storage.local import __create_file, _get_config


def move_physical(project, from_db, to_db):
    path = f'{_get_config()["storage"]["path"]}images/'
    __create_file(f'{path}{to_db}')
    print('Executing file transfer    ', end='\r')
    shutil.move(f'{path}{from_db}/{project}', f'{path}{to_db}')
    print('File transfer executed!    ')


def move_db_entries(project, from_db_name, to_db_name):
    from_peptides, from_db, from_client = __get_client_dbs(from_db_name, 'peptides')
    to_peptides, to_db, to_client = __get_client_dbs(to_db_name, 'peptides')
    query = {'accession': project}
    data = list(from_peptides.find(query))
    [f.update({'_id': f['_id'].replace(from_db_name, to_db_name)}) for f in data]
    print('Executing database transfer   ', end='\r')
    to_peptides.insert_many(data)
    from_peptides.delete_many(query)
    print('Database transfer executed!   ')


def move_data(project, from_db, to_db):
    print('Initializing file transfer', end='\r')
    move_physical(project, from_db, to_db)
    print('Initializing database transfer', end='\r')
    move_db_entries(project, from_db, to_db)