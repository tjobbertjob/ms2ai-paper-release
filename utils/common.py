import bisect


def __validated_input(prompt, valid_values, show_options=True):
    value = input(prompt + ' | ' + ' / '.join(valid_values) + ": ") if show_options else input(prompt)
    while value not in valid_values:
        value = input(prompt + ' | ' + ' / '.join(valid_values) + ": ") if show_options else input(prompt)
    return value


def __get_lower_bound(haystack, needle):
    idx = bisect.bisect(haystack, needle)
    if 0 < idx < len(haystack):
        return idx
    else:
        raise ValueError(f"{needle} is out of bounds of {haystack}")


def all_string_match(string, pattern, overlap=False):
    import re
    if overlap:
        return [m.start() for m in re.finditer('(?=%s)(?!.{1,%d}%s)' % (pattern, len(pattern) - 1, pattern), string)]
    else:
        return [m.start() for m in re.finditer(pattern, string)]