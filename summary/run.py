from collections import Counter
from itertools import chain
from time import sleep

from utils.common import __validated_input
from pymongo import MongoClient


def __print_inital_info():
    client = MongoClient()
    cursor = client.list_databases()

    databases = []
    for db in cursor:
        current_db = client[db['name']]
        current_db_name = db['name']
        if 'peptides' in current_db.list_collection_names():
            storage = round(db['sizeOnDisk'] * 1e-9, 4)
            peptide = current_db.command("collstats", "peptides")["count"]
            try:
                filtered = current_db.command("collstats", "filtered")["count"]
            except:
                filtered = 0
            databases.append(current_db_name)
            print(
                f'DB: {current_db_name}. Storage: {storage} gb. Total peptides: {peptide}, of which {filtered} are currently in a filtered version')

    current_db = MongoClient()['ms2ai']
    storage = round(current_db.command("collstats", "pride")['size'] * 1e-9, 4)
    metadata = current_db.command("collstats", "pride")['count']
    databases.append('pride')
    print(
        f'DB: pride. Storage: {storage} gb. Total project entries: {metadata}. Extend for more information on metadata')

    databases.append('Local data!')

    return __validated_input('\nChoose DB to extend:', databases, False)


def __specific_pride_info(data, query, query_name):
    def __print_option(first, last):
        [print(f'({i + first}) {f[0]}: {f[1]} occurances') for i, f in enumerate(counted_list[first:last])]
    print(f'Calculating!', end="\r")
    query_list = [[f['accession'], g] for f in data.find() if query in f for g in f[query]]
    counted_list = Counter([f[1] for f in query_list]).most_common()
    print(f'Done!', end="\r")
    start_value = 0
    end_value = 10
    __print_option(start_value, end_value)
    possible_options = list(chain.from_iterable([['', 'next'], [str(f) for f in range(0, end_value)]]))
    next_or_back = __validated_input(f'\nOptions: Value for projects with given {query_name}. Press enter to go back. Type "next" to get next elements in the list! ', possible_options, False)
    while next_or_back == 'next':
        start_value = end_value
        end_value = end_value+10
        __print_option(start_value, end_value)
        possible_options = list(chain.from_iterable([['', 'next'], [str(f) for f in range(0, end_value)]]))
        next_or_back = __validated_input(f'\nOptions: Value for projects with given {query_name}. Press enter to go back. Type "next" to get next elements in the list! ', possible_options, False)

    if next_or_back == '':
        __extended_pride_info()
    else:
        relevant_projects = [f[0] for f in query_list if counted_list[int(next_or_back)][0] in f]
        print(f"Projects: {', '.join(relevant_projects)}")
        input('Hit enter to go back!')
        return __extended_pride_info()


def __extended_pride_info():
    data = MongoClient()['ms2ai']['pride']
    print(f'\npride choosen! See list for additional info')
    info_options = ['Species', 'Tissues', 'PTMs', 'MS Instruments', 'Go back!']
    actual_name_list = ['species', 'tissues', 'ptmNames', 'instrumentNames']

    [print(f'({i}) {f}') for i, f in enumerate(info_options)]
    requested_info = __validated_input(f'Request information on:', [str(f) for f in range(len(info_options))], False)

    if int(requested_info) + 1 == len(info_options):
        print(f'\nReturning to DB selection!')
        run_summary()

    __specific_pride_info(data, actual_name_list[int(requested_info)], info_options[int(requested_info)])
    print()


def __resolution_info(peptide_data, database_name):
    print(f'\nMS1 Resolution: {peptide_data.find_one()["ms1size"]} \nMS2 Resolution: {peptide_data.find_one()["ms2size"]}')
    input('Press enter to go back!')
    return __extended_peptide_info(database_name)


def __specific_pep_info(peptides, query):
    print(f'Calculating!', end="\r")
    query_list = [f[query] for f in peptides.find()]
    if isinstance(query_list[0], list):
        query_list = chain.from_iterable(query_list)
    counted_list = Counter(query_list).most_common()
    print(f'Done!', end="\r")
    [print(f'{f[0]}: {f[1]} occurances') for f in counted_list[:10]]


def __extended_peptide_info(database_name):
    db = MongoClient()[database_name]
    peptides = db['peptides']
    print(f'\n{database_name} choosen! See list for additional information')
    info_options = ['Image resolutions', 'Charge states', 'Sequences', 'Sequence lengths', 'Proteins', 'Modifications (PTMs)', 'Go back!']
    actual_name_list = ['', 'Charge', 'Sequence', 'Length', 'Proteins', 'Modifications']
    [print(f'({i}) {f}') for i, f in enumerate(info_options)]
    requested_info = __validated_input(f'Request information on:', [str(f) for f in range(len(info_options))], False)
    if int(requested_info) + 1 == len(info_options):
        print(f'\nReturning to DB selection!')
        run_summary()
    elif requested_info == '0':
        __resolution_info(peptides, database_name)
    else:
        __specific_pep_info(peptides, actual_name_list[int(requested_info)])
    print()


def run_summary():
    requested_db = __print_inital_info()
    if requested_db == 'pride':
        __extended_pride_info()
    else:
        __extended_peptide_info(requested_db)