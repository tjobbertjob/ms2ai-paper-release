import sys
import argparse


def setup_args():
    parser = argparse.ArgumentParser(description='MS2AI Pipeline options')
    parser.add_argument('-s', '--scraper', help='PRIDE metadata scraper', action='store_true')

    parser.add_argument('-f', '--filter', type=str, metavar='', help='Class for filter')
    parser.add_argument('-b', '--binary_filter', type=str, metavar='', help='Specific binary subclass for filter')
    parser.add_argument('-c', '--class_count', type=int, metavar='', help='Amount of classes used for filtering')
    # parser.add_argument('-B', '--balance', type=str, metavar='', help='Create an equal class balance by removing excess data at random', action='store_true')

    parser.add_argument('-e', '--extract', type=str, nargs='+', metavar='', help='Script used for extract api')

    parser.add_argument('-n', '--network', type=str, metavar='', help='In-built example neural network')
    parser.add_argument('-t', '--test', required='--network' in sys.argv, help='Test trained network on the filtered metadata', action='store_true')

    parser.add_argument('-db', '--pride_db', help='Retrieve current PRIDE database version', action='store_true')
    parser.add_argument('-i', '--insert', required='--pride_db' in sys.argv, help='insert local data into pride db for filtering', action='store_true')

    parser.add_argument('-S', '--summary', help='In Depth analysis of available metadata and raw file data', action='store_true')

    parser.add_argument('-T', '--tutorial', help='In-built tutorial to pipeline', action='store_true')

    parser.add_argument('-m', '--move', help='Move data and entries from one db to another', type=str, nargs='+', metavar='')

    return parser


if __name__ == '__main__':
    """
    Main API location for MS2AI.
    
    Runs the entire pipeline given a script to run. 
    Pipeline structure:
        Pride scraper
        Peptide extractor
        Peptide filter
        Peptide neural network
    """
    try:
        parser = setup_args()
        input_args = parser.parse_args()
    except:
        if any(item in ['-h', '--help'] for item in sys.argv):
            quit()
        else:
            parser.print_help()


    if input_args.tutorial:
        from tutorial.tutorial import full_tutorial
        full_tutorial()

    if input_args.summary:
        from summary.run import run_summary
        run_summary()

    if input_args.pride_db:
        from storage.db_init import get_current_pride
        get_current_pride(input_args.insert)

    if input_args.scraper:
        from scraper.metadata import get_metadata
        get_metadata()

    if input_args.extract is not None:
        from extractor.run import run_extractor
        run_extractor(input_args.extract)

    if input_args.filter is not None:
        from filter.run import run_filter
        run_filter(*[input_args.filter, input_args.binary_filter, input_args.class_count])

    if input_args.network is not None:
        from network.run import run_network
        run_network(*[input_args.network, input_args.test])

    if input_args.move is not None:
        from utils.move import move_data
        try:
            project = input_args.move[0]
            from_db = input_args.move[1]
            to_db = input_args.move[2]
        except:
            print('Move order not understood! Make sure to read the documentatiton properly before utilizing this feature!')
            quit()
        move_data(input_args.move[0], input_args.move[1], input_args.move[2])