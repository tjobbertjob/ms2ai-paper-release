import os
from functools import partial
from multiprocessing.pool import Pool
import re
from shutil import copyfile
from zipfile import ZipFile

import pandas as pd
import numpy as np

from extractor.common import __tryexcept_error, __import_table
from extractor.create_images import __create_images
from extractor.format_mzml import __raw_to_mzml_formatter
from extractor.mzml_extractor import __mzML_extractor
from extractor.pride_downloader import __ms2_scan_name
from storage.local import __create_file, _get_config
from utils.log import __internal_print


def __move_local_files(path, filepath, filename, df, pepfile):
    __create_file(f'{path}/')

    if not (os.path.exists(f'{path}file.raw') or os.path.exists(f'{path}file.mzML') or os.path.exists(
            f'{path}mzML.txt')):
        copyfile(f'{filepath}{filename}.raw', f'{path}/file.raw')
    if not os.path.exists(f'{path}{pepfile}'):
        pd.DataFrame.to_csv(df, f'{path}{pepfile}', index=False)


def __local_iterate_rawfiles(filename, df, dirpath, path, accnr, pepfile, multiprocessing):
    try:
        df2 = df.loc[df['Raw file'] == filename,]

        if re.search(path, dirpath) is not None and os.path.exists(f'{path}{dirpath[re.search(path, dirpath).end():]}'):
            filepath = f'{path}{dirpath[re.search(path, dirpath).end():]}'
            accnr = dirpath[re.search(path, dirpath).end():].split('/')[0]
        else:
            filepath = f'{path}{accnr}/{filename}/'
        __move_local_files(filepath, dirpath, filename, df2, pepfile)

        __raw_to_mzml_formatter(accnr, filename, path, filepath, multiprocessing)
        __mzML_extractor(multiprocessing, filepath)

        __create_images(accnr, filename, path, filepath, df2, multiprocessing)
        __internal_print(f'{filename}: ✔', multiprocessing, False)

    except KeyboardInterrupt:
        quit()

    except:
        __tryexcept_error('rawfile', filename, multiprocessing)
        pass


def __mqfile_in_zip(zipfile, pepfile):
    with ZipFile(f'{zipfile}', 'r') as zipObj:
        for f in zipObj.namelist():
            if pepfile in f:
                return zipObj.open(f)


def __local_iterate_zipfiles(file, dirpath, accnr, path, multiprocessing, pepfile):
    try:
        if file.endswith('.txt'):
            df = __import_table(file=f'{dirpath}{file}')
        else:
            zip_mqfile = __mqfile_in_zip(f'{dirpath}{file}', pepfile)
            df = __import_table(file=zip_mqfile, memory_file=True)

        df.replace(' ', np.nan, inplace=True)
        df.dropna(subset=['Sequence', __ms2_scan_name()], inplace=True)
        rawfiles = list(np.unique(df['Raw file']))

        for filename in rawfiles:
            __local_iterate_rawfiles(filename, df, dirpath, path, accnr, pepfile, multiprocessing)

    except KeyboardInterrupt:
        quit()

    except:
        __tryexcept_error('zipfile', dirpath.split("/")[-1], multiprocessing)
        pass


def local_peptide_extractor(dirpath, path, pepfile, multiprocessing=False, from_path=False):
    dirpath = f'{dirpath}'
    if from_path:
        accnr = dirpath.split('/')[-2]
    else:
        accnr = dirpath.split('/')[-3]

    zipfiles = [file for file in os.listdir(f'{dirpath}') if file.endswith('.zip')]
    allpepfiles = [file for file in os.listdir(f'{dirpath}') if file == pepfile]

    all_iterate = zipfiles + allpepfiles

    if from_path and _get_config()['extractor']['multi_processing'] and len(all_iterate) > 1:
        processes = min(_get_config()['extractor']['nr_processes'], len(all_iterate))
        with Pool(processes) as ps:
            ps.map(partial(__local_iterate_zipfiles, dirpath=dirpath, accnr=accnr,
                           path=path, multiprocessing=True, pepfile=pepfile), all_iterate)
    elif from_path:
        [__local_iterate_zipfiles(f, dirpath, accnr, path, False, pepfile) for f in all_iterate]
    else:
        [__local_iterate_zipfiles(f, dirpath, accnr, path, multiprocessing, pepfile) for f in all_iterate]