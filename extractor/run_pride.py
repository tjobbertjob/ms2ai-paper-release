
import glob
import os

import pandas as pd
from simplejson import loads

from extractor.create_images import __create_images
from extractor.format_mzml import __raw_to_mzml_formatter
from extractor.mzml_extractor import __mzML_extractor
from extractor.pride_downloader import __get_mqfile_df, __file_handler
from extractor.pride_finder import __pride_file_finder
from extractor.common import __check_for_skipability, __check_complete_internal, __tryexcept_error, __import_table
from utils.log import __internal_print, ms2ai_log
from storage.local import __create_file


def __iterate_rawfiles(filename, accnr, haveallMQF, path, pepfile, all_raw, multiprocessing, df, summary):
    """
        iterates through all the raw files in the current zipfile.
    """
    try:
        __internal_print(f'file: {accnr}/{filename}', multiprocessing, False)
        if haveallMQF:
            filepath = f'{path}{accnr}/{filename}/'
            df2 = __import_table(file=f'{filepath}{pepfile}')
            summary = loads(open(f'{filepath}summary.txt', 'r').read())
        else:
            df2, filepath = __file_handler(accnr, filename, path, pepfile, df, summary, multiprocessing, all_raw)
            summary = loads(open(f'{filepath}summary.txt', 'r').read())

        __raw_to_mzml_formatter(accnr, filename, path, filepath, multiprocessing)
        __mzML_extractor(multiprocessing, filepath)
        __create_images(accnr, filename, path, filepath, df2, summary, multiprocessing)
        __internal_print(f'{filename}: ✔', multiprocessing, False)

    except KeyboardInterrupt:
        quit()

    except:
        __tryexcept_error('rawfile', filename, multiprocessing)
        pass


def __iterate_zipfiles(zips, haveallMQF, path, accnr, multiprocessing, pepfile, use_summary, all_raw, acquire_only_new):
    """
    iterates through all the zip files in the pride repository
    """
    try:
        (rawfiles, df, summary) = (os.listdir(f'{path}{accnr}'), None, None) if haveallMQF else __get_mqfile_df(zips, multiprocessing, pepfile, use_summary)
        foundrawfiles = [files for files in rawfiles if files in [links[63:-4] for links in all_raw]]

        for filename in foundrawfiles:
            if acquire_only_new and len(glob.glob(f'{path}{accnr}/{filename}/mzML.txt')) != 0:
                continue
            __iterate_rawfiles(filename, accnr, haveallMQF, path, pepfile, all_raw, multiprocessing, df, summary)

        return foundrawfiles

    except KeyboardInterrupt:
        quit()

    except:
        __tryexcept_error('zipfile', zips.split("/")[-1], multiprocessing)
        pass


def peptide_extractor(accnr, path, pepfile, use_summary, acquire_only_new, ram, multiprocessing=False):
    """
    :param accnr: accession or endpath given to api
    :param path: path to the data
    :param pepfile: What maxquant file to use
    :param acquire_only_new: Whether or not to acquire only not-known data or not.
    :param skip_incomplete: Whether or not to acquire only known data or not
    :param multiprocessing: boolean value for whether or not the extractor is multiprocessed
    :param filedata: urls for all known files from the pride links
    :return: extracts images for all peptides in the mq file along with metadata.

    """
    __internal_print(f'\nAccessions: {accnr}', multiprocessing, False)
    __create_file(f'{path}{accnr}/')

    # Find all zip files, rawfiles and find out if we have the MQF on local storage
    (all_zip, all_raw, haveallMQF) = __pride_file_finder(accnr, path, pepfile, use_summary, ram)

    # Check for issues
    if all_raw == True:
        ms2ai_log(f'PRIDE Error - {all_zip}', 'error')
        return

    # skip files if skip-incomplete or acquire_only_new is true
    if __check_for_skipability(haveallMQF, accnr, all_raw, acquire_only_new):
        return

    # Go through all zipfiles
    for zips in all_zip:
        __iterate_zipfiles(zips, haveallMQF, path, accnr, multiprocessing, pepfile, use_summary, all_raw, acquire_only_new)

    __check_complete_internal(path, accnr, all_raw)
