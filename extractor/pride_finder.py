import os
import requests

from storage.local import _get_config


def __zipfile_filter(jsonelem, file_type, type_value, dl_link):
    return type_value == 'SEARCH' and file_type in ['zip'] and dl_link[-9:].lower() != "fasta.zip"


def __rawfile_filter(jsonelem, filetype):
    return filetype == 'RAW'


def get_url_api(url):
    try:
        return requests.get(url).json()

    except:
        return 'serverissue'


def __get_files(url, ram):
    urljson = get_url_api(url)
    if isinstance(urljson, str):
        return urljson, True

    zipfiles = []
    rawfiles = []

    # If zipfiles have the same name as rawfiles and we have the allpeptides, dont download
    try:
        for jsonelem in urljson:
            type_value = jsonelem['fileCategory']['value']
            file_type = jsonelem['fileName'].split('.')[-1]
            dl_link = [f['value'] for f in jsonelem['publicFileLocations'] if f['name'].startswith('FTP')][0]

            if __zipfile_filter(jsonelem, file_type, type_value, dl_link):
                zipfiles.append(dl_link)

            elif __rawfile_filter(jsonelem, type_value):
                rawfiles.append(dl_link)
    except KeyError:
        print('PRIDE API not responding')
        quit()

    return zipfiles, rawfiles


def __check_for_mqfile(path, accnr, rawfiles, pepfile, use_summary):
    allCheck = []

    raw_dirs = [f for f in os.listdir(f'{path}{accnr}/') if not f.startswith('.')]
    if len(raw_dirs) == len(rawfiles):
        for files in raw_dirs:
            dir_contains = os.listdir(f'{path}{accnr}/{files}/')
            pepfile_query = all(item in dir_contains for item in [pepfile, 'summary.txt']) if use_summary else all(item in dir_contains for item in [pepfile])
            if pepfile_query and any(i in ['file.raw', 'file.mzML.gz', 'mzML.txt'] for i in dir_contains):
                allCheck.append(True)
            else:
                allCheck.append(False)
    else:
        return False

    return not False in allCheck


def __pride_file_finder(accnr, path, pepfile, use_summary, ram):
    url = _get_config()['storage']['pride_files'].replace('PXDxxxxxx', accnr)
    (zipfiles, rawfiles) = __get_files(url, ram)

    haveallMQF = __check_for_mqfile(path, accnr, rawfiles, pepfile, use_summary)

    return zipfiles, rawfiles, haveallMQF
