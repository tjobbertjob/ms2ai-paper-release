import io
import os
from copy import copy
from zipfile import ZipFile

import numpy as np
from itertools import chain
import pandas as pd
import glob

from detect_delimiter import detect

from storage.local import _get_config, __get_mqfile


def __check_for_skipability(haveallMQF, accnr, allRaw, acquire_only_new):
    ext_mzml = glob.glob(f'Data/{accnr}/*/mzML.txt')
    if haveallMQF and acquire_only_new and len(ext_mzml) == len(allRaw):
        print(f'{accnr}: ✔ - {len(allRaw)}/{len(allRaw)} Rawfiles extracted')
        return True


def __check_complete_internal(path, accnr, all_raw):
    allCheck = [files for files in os.listdir(f'{path}{accnr}/') if not files.split('/')[-1].startswith('.') and
                'mzML.txt' in os.listdir(f'{path}{accnr}/{files}')]
    progress = '✓' if len(all_raw) == len(allCheck) else '✗' if len(allCheck) == 0 else '~'

    likely_culprit = '' if progress == '✓' else '[Likely culprit: Corrupt zipfile, Rawfile mismatch, or no Seqs/MS2 Scans]' if progress == '✗' else '[Likely culprit: Rawfile mismatch or no Seqs/MS2 Scans]'

    print(f'{accnr} {progress}. Total files: {len(all_raw)}. Extracted files: {len(allCheck)}. {likely_culprit}')


def __tryexcept_error(errortype, filename, multiprocessing):
    if _get_config()['extractor']['receive_traceback'] and not multiprocessing:
        import traceback
        print(f'{errortype} error. {filename}\n', traceback.print_exc())


def __import_table(file, memory_file=False):
    # Reading with csv, if that doesnt work, rewind the iterable and try a csv delimiter
    df = pd.read_csv(file, sep='\t', low_memory=False, error_bad_lines=False)
    if len(df.columns) == 1:
        if memory_file:
            file.seek(0)
        df = pd.read_csv(file, sep=',', low_memory=False, error_bad_lines=False)

    return df



