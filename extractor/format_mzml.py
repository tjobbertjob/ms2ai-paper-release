import os
import stat
import subprocess
from time import time


from sys import platform

from storage.local import _get_config
from utils.log import __internal_print, ms2ai_log


def __save_absolute(path):
    if path[0] == '/':
        return path
    else:
        return f'{os.getcwd()}/{path}'


def __check_conda_thermo():
    from conda.cli.python_api import run_command, Commands
    try:
        return 'thermorawfileparser' in str(run_command(Commands.LIST))
    except:
        ms2ai_log('Conda is not installed - Install conda or change format method', 'error')
        return quit()


def __install_conda_thermo(channel_list):
    from conda.cli.python_api import run_command, Commands
    if not __check_conda_thermo():
        [run_command(Commands.CONFIG, '--add', 'channels', x) for x in channel_list]
        run_command(Commands.INSTALL, '-c', 'bioconda', 'thermorawfileparser')
        if not __check_conda_thermo():
            ms2ai_log('Conda issue. Cannot install thermorawfileparser, try re-installing conda', 'error')
            return quit()


def __run_conda_thermo(relpath, accnr, filename):
    try:
        conda_path = subprocess.check_output('which conda', shell=True).decode('ascii')[:-6]
        peak_picking = '-p' if _get_config()['extractor']['thermo_peak_picking'] else ''
        # start = time()
        os.system(f'mono {conda_path}ThermoRawFileParser.exe'           # Run TRFP from conda path
                  f'      -i="{relpath}{accnr}/{filename}/file.raw" '   # Input path with raw file
                  f'      -o="{relpath}{accnr}/{filename}/" '           # Output path without file name
                  f'      -f=1 -g -m=1 {peak_picking} -l=0')            # .txt metadata file, mzML.gz, peak_picking, and muted.
        # stop = time()
        # print(stop - start)
    except KeyboardInterrupt:
        os.remove(f'{relpath}{accnr}/{filename}/file.mzML.gz')


def __run_docker_thermo(relpath, accnr, filename):
    import docker
    try:
        # start = time()
        mute = '>nul' if platform.startswith('win') else '> /dev/null'
        peak_picking = '-p' if _get_config()['extractor']['thermo_peak_picking'] else ''
        docker.from_env().images.pull('veitveit/trp')
        os.system(f'docker run '
                  f'        -v "{relpath[:-1]}:/data_input" '               # Set path as /data_inputt
                  f'        veitveit/trp ThermoRawFileParser '              # Docker image to run
                  f'        -i="/data_input/{accnr}/{filename}/file.raw" '  # Input path with raw file
                  f'        -o="/data_input/{accnr}/{filename}/" '          # Output path without file name
                  f'        -f=1 -g -m=1 {peak_picking} {mute}')            # .txt metadata file, mzML.gz, peak_picking, and muted.
        # stop = time()
        # print(stop - start)

    except KeyboardInterrupt:
        os.remove(f'{relpath}{accnr}/{filename}/file.mzML.gz')


def __raw_to_mzml_formatter(accnr, filename, path, filepath, multiprocessing):
    """
    :param accnr: PRIDE accession or folder given
    :param filename: self-explanatory
    :param path: path to the data
    :param filepath: Path to the file
    :param multiprocessing: Whether or not we're multiprocessing the extractor
    :return: a formatted version of the raw file to mzML.

    Most important part is to get the conda or docker version of the thermorawfileparser to work.
    """
    __internal_print('Formatting file to mzML', multiprocessing)

    formatusing = _get_config()['extractor']['format_software']
    relpath = __save_absolute(path)

    if os.path.exists(f'{filepath}file.raw') and not (os.path.exists(f'{filepath}file.mzML.gz') or os.path.exists(f'{filepath}mzML.txt')):
        if formatusing == 'conda' and (platform.startswith('linux') or platform.startswith('darwin')):
            __install_conda_thermo(['defaults', 'bioconda', 'conda-forge'])
            __run_conda_thermo(relpath, accnr, filename)

        elif formatusing == 'docker' or not (platform.startswith('linux') or platform.startswith('darwin')):
            if formatusing == 'conda':
                __internal_print('Formatting software changed to Docker due to operating system incompatabilities', multiprocessing, False)
            __run_docker_thermo(relpath, accnr, filename)

        else:
            ms2ai_log('format software not specified in config.json. Choices are "conda" or "docker"', 'error')
            return quit()

        os.rename(f'{filepath}file.mzML.gzip', f'{filepath}file.mzML.gz')  # Using .gz instead of .gzip

        if _get_config()['extractor']['remove_used_files']:
            if os.path.exists(f'{filepath}file-metadata.txt'):
                os.remove(f'{filepath}file-metadata.txt')
            if os.path.exists(f'{filepath}file.raw'):
                os.remove(f'{filepath}file.raw')

