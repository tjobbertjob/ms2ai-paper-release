import os
from functools import partial
from multiprocessing.pool import Pool

import psutil

from extractor.run_local import local_peptide_extractor
from storage.db_init import __get_client_dbs
from utils.common import __validated_input
from extractor.run_pride import peptide_extractor
from storage.local import __reset_peptidedata, _get_config, __get_path, __get_metapath


def __get_params():
    datapath = __get_path()
    metapath = __get_metapath()
    data = _get_config()['extractor']
    pepfile = data['maxquant_file']
    use_summary = data['use_summary_file']
    acquire_only_new = data['acquire_only_new']
    multi = data['multi_processing']
    nr_processes = data['nr_processes']

    return datapath, metapath, pepfile, use_summary, acquire_only_new, multi, nr_processes


def reset_data(db_name=None):
    """
    Removes ALL data from the given data type.
    if a db stucture is chosen, the db is wiped and deleted, and the metadata aswell.
    If dir is chosen, the images folder is removed (SLOW) and the metadata aswell.
    if 'all' is chosen, it iterates through all methods, deleting everything.
    """
    reset = __validated_input('Sure you want to reset peptide data?', ['y', 'n'])
    if reset == 'y':
        __reset_peptidedata(db_name)


def local_data(sysinput, path, pepfile, multi, nr_processes):
    """
    To use this function, put rawfiles and zip/maxquant files into a single directory.
    The program iterates through all zipfiles / maxquant files and handles the rawfiles named in the mq file.
    """
    local_peptide_extractor(sysinput, path, pepfile, from_path=True)
    if True in [os.path.isdir(f'{sysinput}{f}') for f in os.listdir(sysinput)]:
        preacquired_data(sysinput, multi, pepfile, nr_processes)


def pride_project(accessions, path, pepfile, use_summary, acquire_only_new, multi, nr_processes, ram):
    """
    Get data from a single pride accession. Good for specific file retrieval
    """
    if multi and len(accessions) > nr_processes:
        with Pool(nr_processes) as ps:
            ps.map(partial(peptide_extractor, path=path, acquire_only_new=acquire_only_new, pepfile=pepfile,
                           use_summary=use_summary, ram=ram, multiprocessing=True), accessions)
    else:
        [peptide_extractor(accession, path, pepfile, use_summary, acquire_only_new, ram) for accession in accessions]


def pride_data(path, pepfile, acquire_only_new, multi, nr_processes, ram):
    """
    Goes through the entire list of accessions in the pride metadata, filtered or otherwise
    This is the main method of achieving high amounts of data
    """
    pride, db, client = __get_client_dbs('ms2ai', 'pride')
    use_summary = True
    if use_summary:
        pride_filter = {'maxquant_files': {'$all': [pepfile, 'summary.txt']}, 'filetypes': {'$all': ['zip', 'raw']}, 'manual_input': {'$exists': False}}
    else:
        pride_filter = {'maxquant_files': pepfile, 'filetypes': {'$all': ['zip', 'raw']},'manual_input': {'$exists': False}}
    accessions = pride.find(pride_filter).distinct('accession')
    if multi:
        with Pool(nr_processes) as ps:
            ps.map(partial(peptide_extractor, path=path, acquire_only_new=acquire_only_new, pepfile=pepfile,
                           ram=ram, multiprocessing=True), accessions)
    else:
        [peptide_extractor(accession, path, pepfile, acquire_only_new) for accession in accessions]


def preacquired_data(path, multi, pepfile, nr_processes):
    """
    Goes through folders, and gets all directories with the required files to imidiately extract images.
    This is run in order to re-create images in different dimensions, channels, etc.
    Best way to re-create old data.
    """

    def __local_dir_filter(path, pepfile):
        return ('file.mzML.gz' or 'mzML.txt') in os.listdir(path) and pepfile in os.listdir(path)

    acquired = [f'{path}{f}/{g}/' for f in [f for f in os.listdir(f'{path}') if os.path.isdir(f'{path}{f}/') and f not in ['images', 'metadata']]
                for g in os.listdir(f'{path}{f}') if __local_dir_filter(f'{path}{f}/{g}', pepfile)]
    acquired = list(reversed(acquired))
    if multi and len(acquired) > 1:
        with Pool(min(len(acquired), nr_processes)) as ps:
            ps.map(partial(local_peptide_extractor, path=path, pepfile=pepfile, multiprocessing=True), acquired)
    else:
        [local_peptide_extractor(str(accession), path, pepfile) for accession in acquired]


def run_extractor(*args):
    """
    :param cmd: one of 5 different run methods.
    :return: what asked for in cmd

    This function allows for extraction or reset of various types of data.
    reset removes all current peptide data and metadata under the chosen save_method outlined in config.json
    All other methods extracts data from rawfiles given an maxquant file, all just in different methods.
    Local, already acquired data, single pride PROJECT, or list of accessions from the pride scraper.

    see examples/extractor.py
    """
    # Read datapath from config file
    datapath, metapath, pepfile, use_summary, acquire_only_new, multi, nr_processes = __get_params()
    ram = psutil.virtual_memory()[4]

    if not os.path.exists(datapath):
        print('Path given in config.json doesn\'t exist. Please specify working path.')
        quit()

    # Options
    if str(args[0][0]) == 'reset':  # Reset files and folders if you want to remake all images in another setting
        if len(args[0]) > 1:
            reset_data(args[0][1])
        else:
            reset_data()

    elif os.path.isdir(args[0][0]):  # For local fine purposes.
        local_data(args[0][0], datapath, pepfile, multi, nr_processes)

    elif str(args[0][0]).startswith('PXD'):  # For single PRIDE accessions usage
        pride_project(args[0], datapath, pepfile, use_summary, acquire_only_new, multi, nr_processes, ram)

    elif args[0][0] == 'pride':  # Going through the metadata
        pride_data(datapath, pepfile, acquire_only_new, multi, nr_processes, ram)

    elif args[0][0] in ['recreate', 're-create']:  # For re-creating images from already downloaded and parsed files
        preacquired_data(datapath, multi, pepfile, nr_processes)

    else:
        print('option not understood. Valid options are:'
              '\n/path/to/data/         Local data'
              '\nPXD000000              Single PRIDE project'
              '\nPXD000001 PXD000002    List of PRIDE projects'
              '\npride                  PRIDE metadata projects'
              '\nrecreate               Re-create pre-processed raw files'
              '\nreset                  Removes PRs and PR metadata')
        quit()
