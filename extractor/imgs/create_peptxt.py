import os

import numpy as np
from simplejson import load

from extractor.imgs.peptide_filters import filters
from extractor.imgs.ms2_info import __ms2_bin
from extractor.pride_downloader import __ms2_scan_name
from storage.db_init import __get_client_dbs
from storage.serialization import write
from storage.local import _get_config, __get_peptidepath, __create_file
from extractor.imgs.create_pngs import __pep_png_image
from utils.common import __get_lower_bound
from utils.log import __internal_print


def __find_ms2_scan(rows, mzmlfile):
    scan_name = __ms2_scan_name()
    if ';' not in str(rows[scan_name]):
        return int(rows[scan_name])

    else:
        # Get list of scan numbers if there's a ';' separator, otherwise get the single number
        scannumbers = [int(scans) for scans in rows[scan_name].split(';')]

        # Retrieve ms2data from mzml file
        ms2list = [[f, mzmlfile['ms2'][str(f)]['precursor_ion'], max(mzmlfile['ms2'][str(f)]['rt_array'])] for f in
                   scannumbers if len(mzmlfile['ms2'][str(f)]['rt_array']) > 0]

        # Sort ms2data to get highest intensities first
        sortedms2list = sorted(ms2list, key=lambda x: x[2], reverse=True)

        # Get the closest m/z to the target ion from the decreased intensity list (if multiple m/z's are the same this helps)
        closestmz = [f[1] for f in sortedms2list][
            (np.abs(np.array([f[1] for f in sortedms2list]) - rows['m/z'])).argmin()]

        return [f for f in sortedms2list if f[1] == closestmz][0][0]


def __get_neighbourhood(subimage_interval, mzrangelist, rtrangelist, rows, mz_bin, rt_bin):
    mzlen = int(subimage_interval['mz'] / mz_bin)
    rtlen = int(subimage_interval['rt'] / rt_bin)

    mzlower = int(__get_lower_bound(mzrangelist, rows['m/z']) - mzlen)
    mzupper = int(__get_lower_bound(mzrangelist, rows['m/z']) + mzlen)
    rtlower = int(__get_lower_bound(rtrangelist, rows['Retention time']) - rtlen)
    rtupper = int(__get_lower_bound(rtrangelist, rows['Retention time']) + rtlen)

    return mzlen, rtlen, mzlower, mzupper, rtlower, rtupper


def __ms_info(image, mzlower, mzupper, rtlower, rtupper, mzmlfile, ms2scan, ms_binning_info):
    # Draw neighbourhood from large image
    ms1info = [lines[mzlower:mzupper] for lines in image[rtlower:rtupper]]
    ms1size = str([f for f in np.array(ms1info).shape])
    max_ms2_int = max([ints for ints in mzmlfile['ms2'][str(ms2scan)]['rt_array']])

    if ms_binning_info['apply']:
        # Binning the ms2 into 1 of 4 different binning methods
        (ms2info, ms2size) = __ms2_bin(mzmlfile, ms2scan, ms_binning_info, max_ms2_int)

    else:
        # Draw ms2 information from mzml file
        ms2info = [[mz / ms_binning_info['fixed_mz_length'], ints / max_ms2_int] for mz, ints in
                   zip(mzmlfile['ms2'][str(ms2scan)]['m/z_array'], mzmlfile['ms2'][str(ms2scan)]['rt_array'])]
        ms2size = str([f for f in np.array(ms2info).shape])

    return ms1info, ms1size, ms2info, ms2size


def __append_to_metadata(accnr, ms1size, ms2size, rows, db_name, columns, summary, peptides, config):
    # Write metadata to MongoDB
    metadata = {'_id': db_name, 'accession': accnr, 'ms1size': ms1size, 'ms2size': ms2size}
    for f in columns:
        metadata[str(f.replace('.', ''))] = rows[f]

    if config['extractor']['use_summary_file']:
        [metadata.update({f.replace('.', ''): summary[f]}) for f in summary if f not in metadata]

    peptides.insert_one(metadata)


def __iterate_mqfile(df, summary, mzmlfile, accnr, filename, multiprocessing, imagepath, interval, subimage_interval, mz_bin,
                     rt_bin, mzrangelist, rtrangelist, image, savepng, path):
    """
    Go through MaxQuant file (evidence, allpeptides, msms) and iterate through all peptides.
    This function gets the MS2 scan number (to cross-correlate with mzML file), and gets MS2 data from the mzML file.
    It then gets MS1 data from the MS1 image created earlier.
    """
    config = _get_config()
    db_name = config['storage']['db_name']
    peptides, db, client = __get_client_dbs(None, 'peptides')
    for index, pep in df.iterrows():
        if int((index + 1) / int(df.shape[0]) * 100) % 5 == 0:
            __internal_print(f'Creating subimages: {int(((index + 1) / df.shape[0]) * 100)}%', multiprocessing)

        # Find the closest and highest intensity scan in msms scan list
        ms2scan = __find_ms2_scan(pep, mzmlfile)
        pep_db_name = f'{db_name}/{accnr}/{filename}/{ms2scan}'

        if filters(pep, mzmlfile, ms2scan, db_name, peptides, subimage_interval, interval):
            continue

        # Retrieve neighbourhood numbers
        (mzlen, rtlen, mzlower, mzupper, rtlower, rtupper) = __get_neighbourhood(subimage_interval, mzrangelist,
                                                                                 rtrangelist, pep, mz_bin, rt_bin)
        # Retrieve ms info.
        ms_binning_info = _get_config()['extractor']['ms2_binning']
        (ms1info, ms1size, ms2info, ms2size) = __ms_info(image, mzlower, mzupper, rtlower, rtupper, mzmlfile, ms2scan, ms_binning_info)

        # Save image as into storage, and saves the metadata in db
        additional_info = [pep['m/z'] / ms_binning_info['fixed_mz_length'], pep['Retention time']]
        summary.update({'step size': mzmlfile['add_info']['step size'],
                        'norm rt':  pep['Retention time'] / mzmlfile['add_info']['max_rt'],
                        'norm m/z': pep['m/z'] / mzmlfile['add_info']['max_mz']})
        write([ms1info, ms2info, additional_info], f'{imagepath}{pep_db_name}.txt')
        __append_to_metadata(accnr, ms1size, ms2size, pep, pep_db_name, df.columns, summary, peptides, config)

        # Save peptide images as png files
        if savepng:
            __pep_png_image(ms1info, path, pep_db_name)

    client.close()


def __pep_txt_image(accnr, interval, bins, image, resolution, mzmlfile, path, df, summary, subimage_interval,
                    filename, multiprocessing, savepng):
    (mz_bin, rt_bin) = (bins[0], bins[1])
    mzrangelist = [interval['mz']['min'] + i * mz_bin for i in range(int(resolution['x']))]
    rtrangelist = [interval['rt']['min'] + i * rt_bin for i in range(int(resolution['y']))]

    imagepath = __get_peptidepath()
    db_name = _get_config()['storage']['db_name']
    rawfile_imagepath = f'{imagepath}{db_name}/{accnr}/{filename}'
    __create_file(rawfile_imagepath)

    df.reset_index(drop=True, inplace=True)
    __iterate_mqfile(df, summary, mzmlfile, accnr, filename, multiprocessing, imagepath, interval, subimage_interval, mz_bin,
                     rt_bin, mzrangelist, rtrangelist, image, savepng, path)

