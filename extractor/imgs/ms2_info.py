import numpy as np
from utils.common import __get_lower_bound


def __get_info_lists(mzmlfile, noise_removal, ms2scan):
    # Get ms2 info from the mzml file and removes values below the noise threshold.
    def remove_noise(info):
        return float(info) > (noise_removal * max_int)

    intensity_array = mzmlfile['ms2'][str(ms2scan)]['rt_array']
    mz_array = mzmlfile['ms2'][str(ms2scan)]['m/z_array']

    max_int = max(intensity_array)
    int_list = [ints for ints in intensity_array if remove_noise(ints)]
    mz_list = [mz for mz, ints in zip(mz_array, intensity_array) if remove_noise(ints)]

    return max_int, int_list, mz_list


def __ms2_numerical_bin(bin_size, x, y, ms2_mz_cap, hist, max_ms2_int):
    ms2_info = [0] * bin_size
    for i, f in enumerate(x):
        if f > ms2_mz_cap: continue  # Remove all entries with m/z's higher than the cap.

        index_position = __get_lower_bound(hist[1], int(f))

        if ms2_info[index_position] == 0:
            ms2_info[index_position] = y[i]
        else:
            ms2_info[index_position] = np.mean([ms2_info[index_position], y[i]])
    return [f / max_ms2_int if f != 0 else 0 for f in ms2_info]


def __ms2_binary_bin(hist):
    ms2_info = hist[0]
    return [1 if hist != 0 else 0 for hist in ms2_info]


def __ms2_bin(mzmlfile, ms2scan, ms2_binning, max_ms2_int, noise_removal=0.05):
    max_int, int_list, mz_list = __get_info_lists(mzmlfile, noise_removal, ms2scan)
    hist_bins = ms2_binning['bin_amount']
    end_range = ms2_binning['fixed_mz_length'] if ms2_binning['fixed_mz'] else max(mz_list)
    hist = np.histogram(mz_list, bins=hist_bins, range=(0, end_range))
    if ms2_binning['retain_int']:  # With intensity. Means all intensities and normalizes intensities of the ms2 spectrum
        ms2_info = __ms2_numerical_bin(hist_bins, mz_list, int_list, ms2_binning['fixed_mz_length'], hist, max_ms2_int)

    else:  # Without intensity
        ms2_info = __ms2_binary_bin(hist)

    ms2_size = str([f for f in np.array(ms2_info).shape])

    return ms2_info, ms2_size