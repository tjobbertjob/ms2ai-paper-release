import numpy as np

from utils.log import __internal_print
from extractor.imgs.create_pngs import __full_png_image
from storage.serialization import write


def iterate_mzml(mzmlfile, interval, rt_bin, mz_bin, maxint):
    # Create an empty array for later use
    ms1_array = {}
    # Get sorted list of scan ids.
    scan_ids = [int(scan_id) for scan_id in mzmlfile['ms1']]

    for scan_id in sorted(scan_ids):
        ms1_id = str(scan_id)

        # Get the intervals
        scan_time = float(mzmlfile['ms1'][ms1_id]['scan_time'])
        if scan_time < interval['rt']['min'] or scan_time > interval['rt']['max']: continue

        # Calculate the y axis.
        y_n = int((scan_time - interval['rt']['min']) / rt_bin)
        divisible_value = np.log(maxint)
        for index, mz_elem in enumerate(mzmlfile['ms1'][ms1_id]['mz']):
            if mz_elem < interval['mz']['min'] or mz_elem > interval['mz']['max']: continue
            x_n = int((mz_elem - interval['mz']['min']) / mz_bin)
            _key = (x_n, y_n)
            # Current strategy for collapsing the intensity values is taking their logs
            intensity_val = np.log(max(mzmlfile['ms1'][ms1_id]['intensity'][index], 1)) / divisible_value
            try:
                ms1_array[_key].append(intensity_val)
            except KeyError:
                ms1_array[_key] = [intensity_val]

    return ms1_array


def create_image_list(resolution, ms1_array, multiprocessing):
    image = []
    for y_i in range(0, resolution['y']):
        if y_i % 50 == 0:
            __internal_print(f'Creating full image: {round(y_i / resolution["y"] * 100, 2)}%', multiprocessing)

        row = []
        for x_i in range(0, resolution['x']):
            _key = (x_i, y_i)
            try:
                (meanintensity, minintensity, maxintensity, inputpoints) = \
                    (np.mean(ms1_array[_key]), min(ms1_array[_key]), max(ms1_array[_key]), np.log(len(ms1_array[_key])))
                pixelpoint = [meanintensity, minintensity, maxintensity, inputpoints]
            except KeyError:
                pixelpoint = [0, 0, 0, 0]
            row.append(pixelpoint)
        image.append(row)
    return image


def full_txt_image(mzmlfile, interval, bins, resolution, filepath, savepng, maxint, multiprocessing):
    (mz_bin, rt_bin) = (bins[0], bins[1])
    ms1_array = iterate_mzml(mzmlfile, interval, rt_bin, mz_bin, maxint)

    # Create the final image.
    image = create_image_list(resolution, ms1_array, multiprocessing)
    imagedata = [image, interval, bins, resolution, maxint]

    if savepng:  # save full image to png
        __full_png_image(image, filepath, resolution, interval)

    # Save as txt file
    __internal_print('Saving image files', multiprocessing)
    write(imagedata, f'{filepath}{str(resolution["x"])}x{str(resolution["y"])}.txt')

    return image
