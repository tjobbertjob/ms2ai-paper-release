

def __skip_existing_pepfiles(peptides, db_name):
    if peptides.find_one({'_id': db_name}) is None:
        return False
    else:
        return True


def __skip_outofbounds(rows, subimage_interval, interval):
    return rows['Retention time'] - subimage_interval['rt'] < interval['rt']['min'] or rows['Retention time'] + \
           subimage_interval['rt'] > interval['rt']['max'] or rows['m/z'] - subimage_interval['mz'] < \
           interval['mz']['min'] or rows['m/z'] + subimage_interval['mz'] > interval['mz']['max']


def __peptide_filter(pep, mzmlfile, ms2scan):
    mzint = int(pep['m/z'])
    return not mzint - 2 < mzmlfile['ms2'][str(ms2scan)]['precursor_ion'] < mzint + 2


def filters(pep, mzmlfile, ms2scan, db_name, database, subimage_interval, interval):
    """
    All the filters for each peptide. Existing peptide, Out of bounds and a specific peptide filter, specificed by the user.
    The user specific filter should be configured in the __peptide_filter. Currently the filter removes any peptides
    whose mass does not fall within +-2 m/z of the precursor ions mass.
    """
    if __skip_existing_pepfiles(database, db_name):
        return True

    if __skip_outofbounds(pep, subimage_interval, interval):
        return True

    if __peptide_filter(pep, mzmlfile, ms2scan):
        return True

    return False
