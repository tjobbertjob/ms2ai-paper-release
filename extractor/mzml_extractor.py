import gzip
import os
import sys

from pyteomics import mzml

from storage.local import _get_config
from utils.log import __internal_print, ms2ai_log
from storage.serialization import write


def __process_ms1(spectrum):
    # Scan information
    scan_info = spectrum['scanList']
    # Time
    scan_time = scan_info['scan'][0]['scan start time']
    mz = spectrum['m/z array']
    # ion intensity
    intensity = spectrum['intensity array']

    min_mz = spectrum['lowest observed m/z']
    max_mz = spectrum['highest observed m/z']
    rt = scan_time
    return {'scan_time': scan_time, 'intensity': intensity.tolist(), 'mz': mz.tolist()}, [min_mz, max_mz, rt]


def __process_ms2(spectrum):
    # Fish out the precursors.
    try:
        precursors = spectrum['precursorList']
        if precursors['count'] != 1:
            ms2ai_log('Number of precursors different than 1, not designed for that', 'error')
            quit()
        ion = precursors['precursor'][0]['selectedIonList']
        if ion['count'] != 1:
            ms2ai_log('More than one selected ions, not designed for that', 'error')
            quit()

        ion = ion['selectedIon'][0]['selected ion m/z']
        ms1_scan = int(precursors['precursor'][0]['spectrumRef'].split('scan=')[1])

        # Get the scan index
        scan_index = spectrum['index']
        # Get m/z and intensity arrays
        mz = spectrum['m/z array']
        intensity = spectrum['intensity array']
    except:
        scan_index = []
        ion = []
        ms1_scan = []
        mz = []
        intensity = []

    return {'scan_index': scan_index, 'precursor_scan': ms1_scan, 'precursor_ion': ion, 'm/z': mz, 'rt': intensity}


def create_extraction_dict(data):
    """
    :param data: mzML file
    :param multiprocessing: boolean value for whether or not the extractor is multiprocessed
    :return: a dictionary of ms1 and ms2 data extracted from the mzml file.

    Goes through at either process_ms1 level or process_ms2 level and extract the needed data into a dictionary of
    retention times along with the (int, mz) pairs
    """
    # Extracted data
    extracted = {'ms1': {}, 'ms2': {}, 'add_info': {}}
    # Extract the necessary data from spectra
    min_rt, max_rt, min_mz, max_mz = sys.maxsize, -sys.maxsize, sys.maxsize, -sys.maxsize

    for spectrum in data:
        scan_id = int(spectrum['id'].split('scan=')[1])
        if spectrum['ms level'] == 1:
            # Get information from MS1
            ms1_spectrum, info_list = __process_ms1(spectrum)

            min_rt = float(min(info_list[2], min_rt))
            max_rt = float(max(info_list[2], min_rt))
            min_mz = float(min(info_list[0], min_mz))
            max_mz = float(max(info_list[1], max_mz))

            extracted['ms1'][str(scan_id)] = {'mz': ms1_spectrum['mz'],
                                              'intensity': ms1_spectrum['intensity'],
                                              'scan_time': ms1_spectrum['scan_time']}

        elif spectrum['ms level'] == 2:
            # Get information from MS1
            ms2_spectrum = __process_ms2(spectrum)
            extracted['ms2'][str(scan_id)] = {'scan_index': ms2_spectrum['scan_index'],
                                              'precursor_scan': ms2_spectrum['precursor_scan'],
                                              'precursor_ion': ms2_spectrum['precursor_ion'],
                                              'm/z_array': [mz for mz in ms2_spectrum['m/z']],
                                              'rt_array': [rt for rt in ms2_spectrum['rt']]}

    add_info = {'min_mz': min_mz, 'max_mz': max_mz, 'min_rt': min_rt, 'max_rt': max_rt, 'step size': round(max_rt / (len(extracted['ms1']) + len(extracted['ms2'])), 6)}
    extracted['add_info'].update(add_info)
    return extracted


def __mzML_extractor(multiprocessing, filepath):
    """
    :param multiprocessing: boolean value for whether or not the extractor is multiprocessed
    :return: an extracted version of the mzML file with only ms1 and ms2data
    """
    # Extract the data from the mzml, if we havnt already
    if not os.path.exists(f'{filepath}mzML.txt') and (os.path.exists(f'{filepath}file.mzML') or os.path.exists(f'{filepath}file.mzML.gz')):
        with gzip.open(f'{filepath}file.mzML.gz', 'rb') as gzipfile:
            data = mzml.MzML(gzipfile)
            __internal_print('Extracting data from mzML', multiprocessing)

            extracted = create_extraction_dict(data)

        write(extracted, f'{filepath}mzML.txt')
        if _get_config()['extractor']['remove_used_files']:
            os.remove(f'{filepath}file.mzML.gz')
