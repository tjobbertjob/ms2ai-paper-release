import glob
import os
from time import time

from storage.serialization import read
from utils.log import __internal_print
from extractor.imgs.create_fulltxt import full_txt_image
from extractor.imgs.create_peptxt import __pep_txt_image
from storage.local import _get_config


def __image_preparameters(filepath, multiprocessing):
    __internal_print('Preparing parameter for image creation', multiprocessing)
    mzml = read(f'{filepath}mzML.txt')

    intlist = [item for f in mzml['ms1'] for item in mzml['ms1'][f]['intensity']]

    min_mz, max_mz, min_rt, max_rt, scan_time = list(mzml['add_info'].values())

    interval = {
        'mz': {'min': min_mz, 'max': max_mz},
        'rt': {'min': min_rt, 'max': max_rt}
    }

    # Define the intervals for the given resolution
    (mz_bin, rt_bin) = (_get_config()['extractor']['mz_bin'], _get_config()['extractor']['rt_bin'])

    resolution = {'x': int((max_mz - min_mz) / mz_bin), 'y': int((max_rt - min_rt) / rt_bin)}

    return mzml, interval, [mz_bin, rt_bin], resolution, max(intlist)


def __create_images(accnr, filename, path, filepath, df2, summary, multiprocessing):
    config = _get_config()['extractor']
    savepng = config['save_images_as_png']

    mzml, interval, bins, resolution, maxint = __image_preparameters(filepath, multiprocessing)
    if os.path.exists(f'{filepath}{resolution["x"]}x{resolution["y"]}.txt'):
        (image, interval, bins, resolution, max_int) = read(f'{filepath}{resolution["x"]}x{resolution["y"]}.txt')
    else:
        image = full_txt_image(mzml, interval, bins, resolution, filepath, savepng, maxint, multiprocessing)

    subimage_interval = {'mz': config['mz_interval'], 'rt': config['rt_interval']}
    __pep_txt_image(accnr, interval, bins, image, resolution, mzml, path, df2, summary, subimage_interval,
                    filename, multiprocessing, savepng)
