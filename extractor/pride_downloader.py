import json
import os
from urllib.request import urlretrieve

import numpy as np
import pandas as pd
from simplejson import dump

from extractor.common import __import_table
from utils.log import __internal_print
from storage.local import __get_mqfile, __create_file, _get_config


def __ms2_scan_name():
    pepfile = _get_config()['extractor']['maxquant_file']

    if pepfile == 'allPeptides.txt':
        return 'MSMS Scan Numbers'

    elif pepfile == 'evidence.txt':
        return 'MS/MS Scan Number'

    elif pepfile == 'msms.txt':
        return 'Scan number'


def __get_mqfile_df(zipfile, multiprocessing, mqfile, use_summary):
    # Handle spaces in urls
    zipfileurl = zipfile.translate(str.maketrans({"(": r"\(", ")": r"\)", " ": r"%20"}))

    __internal_print('Retrieving the maxquant file from zipfile', multiprocessing)
    # Extract the dataframe from the maxquant files
    zipfile = __get_mqfile(zipfileurl, mqfile)
    df = __import_table(file=zipfile[0], memory_file=True)
    if use_summary:
        summary = __import_table(file=zipfile[1], memory_file=True)
    df.replace(' ', np.nan, inplace=True)
    df.dropna(subset=['Sequence', __ms2_scan_name()], inplace=True)
    # Get the rawfiles from the maxquant files
    rawfiles = list(np.unique(df['Raw file']))
    return (rawfiles, df, summary) if use_summary else (rawfiles, df, {})


def __raw_file_downloader(filepath, allRaw, filename):
    # Download raw file
    if not (os.path.exists(f'{filepath}file.mzML.gz') or os.path.exists(f'{filepath}mzML.txt')):
        for rawfiles in allRaw:
            if filename in rawfiles:
                urlretrieve(rawfiles, f'{filepath}file.raw')
                break


def __file_handler(accnr, filename, path, maxquant_file, df, summary, multiprocessing, allRaw):
    filepath = f'{path}{accnr}/{filename}/'
    # Make the file directory if it doesnt exist
    __create_file(filepath)

    # Gets rows of allpeptides that corresponds with given filename and save the part of the allpeptides that belong to that file in the storage system
    df2 = df.loc[df['Raw file'] == filename,]
    pd.DataFrame.to_csv(df2, f'{filepath}{maxquant_file}', index=False)

    with open(f'{filepath}summary.txt', 'w') as summary_file:
        summary_dict = summary.loc[summary['Raw file'] == filename].iloc[0].to_dict()
        [summary_dict.update({f: int(summary_dict[f])}) for f in summary_dict if isinstance(summary_dict[f], np.int64)]
        if 'Fixed modifications' not in summary_dict:
            summary_dict['Fixed modifications'] = 'Not available'
        dump(summary_dict, summary_file)

    # Download the raw file
    __internal_print('Downloading raw file', multiprocessing)
    __raw_file_downloader(filepath, allRaw, filename)

    return df2, filepath
