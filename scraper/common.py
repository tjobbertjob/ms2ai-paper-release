from time import sleep
from urllib import parse

import requests
from bs4 import BeautifulSoup as bs


def __get_json_page(url, sleep_time=4.5):
    # 4.5 chosen specifically to always avoid oversaturating the server and sending us into timeout
    try:
        return __get_page(url, sleep_time).json()
    except Exception as error:
        print(error)
        sleep(5)
        return __get_json_page(url, sleep_time)


def __get_page(url, sleep_time):
    try:
        sleep(sleep_time)
        return requests.get(url)
    except Exception as error:
        print('put in timeout', error)
        sleep(5)
        return __get_page(url)


def __get_soup(url, sleep_time=0, parser='html.parser'):
    try:
        page = __get_page(url, sleep_time).text
        return bs(page, parser)
    except:
        return __get_soup(url, sleep_time)


