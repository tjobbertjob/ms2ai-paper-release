from functools import partial
from itertools import chain
from multiprocessing.pool import Pool
from time import time

from pymongo import MongoClient

from scraper.accessions import get_accessions
from scraper.common import __get_json_page
from storage.db_init import __get_client_dbs
from storage.serialization import read
from storage.local import _get_config, __get_zipfilelist
import numpy as np

from utils.common import __validated_input
from utils.log import ms2ai_log
from utils.pride_api import project_api, file_api, get_api_filetypes, get_api_zipfiles


def __mqfile_check(zipfiles, dictionary):
    if 'maxquant' in str(dictionary).lower():
        try:
            smallest_zip = sorted(zipfiles, key=lambda x: x[1], reverse=True)[0][0]
            files_in_zip = __get_zipfilelist(smallest_zip)
            return [True, [f for f in files_in_zip if f != '']]
        except:
            return [True, []]
    else:
        return [False, []]


def __collect_metadata(accession):
    meta_dict = {}
    try:
        meta_dict.update(project_api(accession))
        file_json = file_api(accession)
        filetypes = get_api_filetypes(file_json)
        zipfiles = get_api_zipfiles(file_json)

        maxquant_check = __mqfile_check(zipfiles, meta_dict)
        meta_dict.update({'filetypes': filetypes})
        meta_dict.update({'maxquant': maxquant_check[0]})
        if maxquant_check[1]:
            meta_dict.update({'maxquant_files': maxquant_check[1]})

        return meta_dict

    except Exception as error:
        meta_dict = {'accession': accession, 'status': 'broken'}
        return meta_dict


def __mp_metadata(poolsize, data_list, pride):
    with Pool(poolsize) as ps:
        for i, results in enumerate(ps.imap_unordered(partial(__collect_metadata), reversed(data_list))):
            print(f'{i} / {len(data_list)}', end='\r')
            if results is not None:
                try:
                    # In case the results are above 16mb, the MongoDB refuses to store it.
                    pride.insert_one(results)
                except:
                    pass


def __update_metadata(pride):
    accessions_list = get_accessions()
    existing_accessions = [f['accession'] for f in pride.find()]
    missing_accessions = [f for f in accessions_list if f not in existing_accessions]

    ms2ai_log('-Updating PRIDE project accession metadata-', 'info')
    ms2ai_log(f'-{len(missing_accessions)} new accession numbers-', 'info')
    __mp_metadata(1, missing_accessions, pride)


def get_metadata():
    pride, db, client = __get_client_dbs('ms2ai', 'pride')
    if pride.count() != 0:
        if __validated_input('Want to overwrite last PRIDE metadata file? ', ['y', 'n']) == 'n':
            __update_metadata(pride)
            quit()
        else:
            pride.drop()

    accessions_list = get_accessions()
    ms2ai_log('-Gathering metadata on all PRIDE project accessions-', 'info')
    __mp_metadata(8, accessions_list, pride)
