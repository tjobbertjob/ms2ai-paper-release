import collections
from urllib.request import urlopen
from xml.etree import ElementTree as ET

import xmltodict


def __get_filename(file):
    return file['fileName']


def __get_download_link(file):
    index = 0 if file['publicFileLocations'][0]['name'] == 'FTP Protocol' else 1
    return file['publicFileLocations'][index]['value']


def __flatten_xml(init_dict, parent_key='', sep='/'):
    items = []
    for k, v in init_dict.items():
        # new_key = k
        new_key = parent_key if k == 'string' else parent_key + sep + k  if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(__flatten_xml(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))

    return dict(items)


def __output_xmls(xml_dict):
    keys_of_iterest = ['fastaFilePath', 'pvalThres', 'includeContaminants', 'matchBetweenRuns', 'ibaq', 'top3',
                       'minPepLen', 'peptideFdr', 'proteinFdr', 'siteFdr', 'maxCharge', 'minPeakLen', 'enzymes',
                       'fixedModifications', 'variableModifications', 'MatchTolerance', 'MatchToleranceInPpm',
                       'DeisotopeTolerance', 'DeisotopeToleranceInPpm', 'filePaths']

    output_xml = []
    for i, xml in enumerate(xml_dict):
        inter_dict = {}
        for keys in xml:
            if keys.split('/')[-1] in keys_of_iterest:
                if keys.split('/')[-1] == 'filePaths':
                    inter_dict['files'] = [f.split('\\')[-1].split('/')[-1] for f in xml[keys]]
                else:
                    inter_dict[keys.split('/')[-1]] = xml[keys]
        output_xml.append(inter_dict)

    return output_xml


def __xml_parser(zipfile, files_in_zip, file_json):
    file_location = [f for f in files_in_zip if 'xml' in f]
    if file_location:
        xml_locations = [files.orig_filename for files in zipfile.filelist]
        xml_files = [zipfile.open(f).read() for f in xml_locations if 'xml' in f]

    dl_links = [__get_download_link(f) for f in file_json if 'xml' in __get_filename(f)]
    if dl_links:
        xml_links = [f for f in dl_links if 'xml' in f]
        raw_xml_files = [ET.fromstring(urlopen(f).read()) for f in xml_links]
        xml_files = [ET.tostring(f, encoding='utf-8', method='xml') for f in raw_xml_files]

    xml_dict = [dict(xmltodict.parse(f)) for f in xml_files]
    xml_dict = [__flatten_xml(f['MaxQuantParams']) for f in xml_dict]
    output_xmls = __output_xmls(xml_dict)

    return output_xmls
