from functools import partial
from itertools import chain
from multiprocessing.pool import Pool

from scraper.common import __get_soup
from storage.local import _get_config
from utils.log import ms2ai_log
import re


def get_links(url, absolute=True, predicate=lambda x: x):
    bs_page = __get_soup(url, sleep_time=0).find_all('a', attrs={'href': predicate})
    return [f'{url}{links["href"]}' if absolute else links['href'][:-1] for links in bs_page]


def __filter_l1(s):
    return '20' in s


def __filter_l2(s):
    return re.match(re.compile('[0-9][0-9]'), s[0:2])


def __filter_l3(s):
    return len(s.replace('/', '')) == 9  # length of scraper accessions


def get_accessions():
    ms2ai_log('-Gathering all PRIDE project accessions-', 'info')

    print('Getting years')
    l1_links = get_links(_get_config()['storage']['pride_archive'], predicate=__filter_l1)

    print('Getting months')
    with Pool(8) as ps:
        l2_links = list(chain.from_iterable(ps.map(partial(get_links, predicate=__filter_l2), l1_links)))

    print('Getting all projects')
    with Pool(8) as ps:
        l3_links = list(chain.from_iterable(ps.map(partial(get_links, absolute=False, predicate=__filter_l3), l2_links)))

    return l3_links
