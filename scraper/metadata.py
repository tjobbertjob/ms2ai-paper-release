from functools import partial
from itertools import chain
from multiprocessing.pool import Pool
from time import time

from pymongo import MongoClient

from scraper.accessions import get_accessions
from scraper.common import __get_json_page
from scraper.xml_parser import __xml_parser
from storage.db_init import __get_client_dbs
from storage.serialization import read
from storage.local import _get_config, __get_zipfilelist, __get_zipfile
import numpy as np

from utils.common import __validated_input
from utils.log import ms2ai_log
from utils.pride_api import project_api, file_api, get_api_filetypes, get_api_zipfiles


def get_metadata_page(page_nr):
    return __get_json_page(f'https://www.ebi.ac.uk/pride/ws/archive/v2/projects?page={page_nr}&sortDirection=DESC&sortConditions=submissionDate', sleep_time=0)['_embedded']['projects']


def __metadata_base():
    info = __get_json_page(
        'https://www.ebi.ac.uk/pride/ws/archive/v2/projects?page=1&sortDirection=DESC&sortConditions=submissionDate',
        sleep_time=0)['page']
    page_amount = info['totalPages']
    project_amount = info['totalElements']
    metadata = []

    with Pool(16) as ps:
        for i, pages in enumerate(ps.imap_unordered(get_metadata_page, list(range(0, page_amount)))):
            print(f'Pulling base metadata: {i*100}/{project_amount}', end='\r')
            metadata.append(pages)

    metadata = list(chain.from_iterable(metadata))
    [f.update({'maxquant': 'maxquant' in str(f).lower()}) for f in metadata]
    print(f'Base metadata pulled!                             ')
    return metadata


def __metadata_files(entry):
    file_json = file_api(entry['accession'], sleep_time=0)
    filetypes = get_api_filetypes(file_json)
    entry.update({'filetypes': filetypes})
    zipfiles = get_api_zipfiles(file_json, ['zip'])
    if entry['maxquant'] and zipfiles:
        smallest_zip = sorted(zipfiles, key=lambda x: x[1], reverse=True)[0][0]
        size = sorted(zipfiles, key=lambda x: x[1], reverse=True)[0][1]
        if size*1e-9 > 100:
            print(f'{entry["accession"]}: Zipfile size: {round(size*1e-9, 2)}gb.')
            entry.update({'maxquant_files': 'error: zipfile size'})
            return entry
        # print(entry['accession'], size*1e-9)
        try:
            zipfile = __get_zipfile(smallest_zip)
            entry.update({'maxquant_files': list(np.unique([files.orig_filename.split('/')[-1] for files in zipfile.filelist]))})
            entry.update({'xmls': __xml_parser(zipfile, [f.orig_filename for f in zipfile.filelist], file_json)})
        except:
            entry.update({'maxquant_files': 'error: unknown zipfile error'})

    elif entry['maxquant'] and 'zip' not in filetypes:
        entry.update({'maxquant_files': 'error: No .zip file!'})

    return entry


def get_metadata():
    pride, db, client = __get_client_dbs('ms2ai', 'pride')
    pride.delete_many({'status': 'broken'})

    if pride.count() != 0 and __validated_input('Overwrite current PRIDE metadata? ', ['y', 'n']) == 'y':
        pride.drop()

    metadata = __metadata_base()
    existing = pride.distinct('accession')
    metadata = [f for f in metadata if f['accession'] not in existing]
    print(f'{len(metadata)} new entries!')

    with Pool(min(len(metadata), 16)) as ps:
        for i, results in enumerate(ps.imap_unordered(__metadata_files, metadata)):
            print(f'Pulling file metadata: {i} / {len(metadata)}    ', end='\r')
            if results is not None:
                try:
                    # In case the results are above 16mb, the MongoDB refuses to store it.
                    pride.insert_one(results)
                except:
                    pass
    print('File metadata pulled!                   ')


