import msgpack


def __to_byte(obj):
    return msgpack.packb(obj)


def __from_byte(obj):
    return msgpack.unpackb(obj, raw=False)


