import errno
import os
import shutil
import stat
from io import BytesIO
from urllib.request import urlretrieve, urlopen
from zipfile import ZipFile

from simplejson import loads

import storage.db_init


def _get_config():
    return loads(open('config.json').read())


def __create_file(path):
    if not os.path.exists(path):
        os.makedirs(path)


def __get_path():
    return _get_config()['storage']['path']


def __get_metapath():
    return __get_path() + 'metadata/'


def __get_peptidepath():
    return __get_path() + 'images/'


def __reset_peptidedata(db_name=None):
    datapath = __get_path()
    if db_name is None:
        db_name = _get_config()['storage']['db_name']
    peptides, filtered, db, clients = storage.db_init.__get_client_dbs(db_name, 'peptides', 'filtered')

    if os.path.exists(f'{datapath}images/{db_name}/'):
        try:
            shutil.rmtree(f'{datapath}images/{db_name}/', ignore_errors=False, onerror=handleRemoveReadonly)
        except:
            print(f'Cannot delete files from {datapath}images/{db_name}/. If the directory is open, close it and re-try')
            quit()

    peptides.drop()
    filtered.drop()


def handleRemoveReadonly(func, path, exc):
    excvalue = exc[1]
    if func in (os.rmdir, os.remove) and excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)  # 0777
        func(path)

    else:
        raise


def __get_rawfile(url, path):
    return urlretrieve(url, path)  # Store raw file in storage
    # return BytesIO(urlopen(url).read())  # Store raw file in memory


def __get_zipfile(url):
    return ZipFile(BytesIO(urlopen(url).read()))


def __get_zipfilelist(url):
    zipfile = __get_zipfile(url)
    return [files.orig_filename.split('/')[-1] for files in zipfile.filelist]


def __get_mqfile(url, mqfile):
    zipfile = __get_zipfile(url)
    return [zipfile.open(files) for files in zipfile.filelist if files.orig_filename.split('/')[1] in [mqfile, 'summary.txt']]
