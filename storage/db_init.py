import os
from urllib.request import urlretrieve

from scraper.common import __get_json_page
import storage.local
from utils.common import __validated_input


def __get_client_dbs(db_name, *args):
    if db_name is None:
        db_name = storage.local._get_config()['storage']['db_name']

    from pymongo import MongoClient
    client = MongoClient()
    db = client[db_name]

    cols = [db[str(f)] for f in args]
    cols.append(db)
    cols.append(client)

    return cols


def insert_to_pride_db():
    a = __get_json_page(f'{storage.local._get_config()["storage"]["pride_project"]}PXD010595/', sleep_time=0).keys()
    print('Input values for the project below. Leave empty if non-applicable')
    b = {f: input(f'project {f}: ') if f != 'accession' else input(f'project name (accession): ') for f in a}
    c = {f: b[f] for f in b if b[f]}
    c.update({'manual_input': True})
    pride, client, db = __get_client_dbs('ms2ai', 'pride')
    pride.insert_one(c)


def get_current_pride(insert_to_db):
    if not insert_to_db:
        db_link = storage.local._get_config()['storage']['current_pride_db']
        urlretrieve(db_link, 'pride.bson.gz')
        # mongodump --archive=pride.bson.gz --gzip --db ms2ai --collection pride
        os.system('mongorestore --gzip --archive=pride.bson.gz --db ms2ai')
        os.remove('pride.bson.gz')
    else:
        insert_to_pride_db()