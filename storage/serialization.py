import os
from gzip import GzipFile

from storage.byte_manager import __to_byte, __from_byte


def write(obj, name):
    with open(name, 'wb') as file:
        return file.write(__to_byte(obj))


def read(name):
    with open(name, 'rb') as file:
        return __from_byte(file.read())


def append_write(obj, name):
    if os.path.exists(name):
        read_file = read(name)
        read_file.update(obj)
        obj = read_file
    write(obj, name)